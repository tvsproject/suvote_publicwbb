/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

/**
 * Enum that specifies the different types of Error that could occur during a commit run
 * 
 * @author Chris Culnane
 * 
 */
public enum PublicWBBCommitError {
  FAILED_VALIDATION, SIGNATURE_FAILURE, DUPLICATE_ENTRY, INVALID_MESSAGE, INVALID_FILE
}
