/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

/**
 * Constants class for database fields
 * 
 * @author Chris Culnane
 * 
 */
public class DBFields {

  /**
   * Collections within the database.
   */
  public static class Collections {

    public static final String COMMITS = "commits";
  }

  // Useful database strings.

  public static final String ID                    = "_id";
  public static final String LESS_THAN             = "$lt";
  public static final String NOT_EQUAL             = "$ne";
  public static final String GREATER_THAN_EQUAL_TO = "$gte";
  public static final String SET                   = "$set";
  public static final String EXISTS                = "$exists";
  public static final String INCREMENT             = "$inc";
  public static final String ADD_TO_SET            = "$addToSet";
  public static final String UNSET                 = "$unset";
  public static final String NOT                   = "$not";
  public static final String ELEMENT_MATCH         = "$elemMatch";
  public static final String PULL                  = "$pull";

  public static final String DATA_PATH             = "dataPath";
  public static final String DATA_PATH_ATTACHMENTS = "dataPathAttach";
  public static final String FROM_PEER             = "fromPeer";
  public static final String VALID_MSGS_RECEIVED   = "validReceived";
  public static final String MSG                   = "msg";
  public static final String COMMIT_DATA_FILES     = "commitDataFiles";
  public static final String DATA_HASH             = "hash";
  public static final String FILE_PATH_JSON        = "filePathJSON";
  public static final String FILE_PATH_ATTACHMENTS = "filePathAttachments";
  public static final String JOINT_SIGNATURE       = "signature";

}
