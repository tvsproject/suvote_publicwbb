/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

/**
 * Constants class for the PublicWBB
 * 
 * @author Chris Culnane
 * 
 */
public class PublicWBBConstants {

  /**
   * Constants for the Config file of a PublicWBB
   */
  public static class Config {

    public static final String CERTIFICATE_KEYSTORE     = "certificateKeyStore";
    public static final String CERTIFICATE_KEYSTORE_PWD = "certificateKeyStorePwd";
    public static final String KEYSTORE_PATH            = "keyStorePath";
    public static final String KEYSTORE_PWD             = "keyStorePwd";
    public static final String LISTEN_PORT              = "listenPort";
    public static final String BUFFER_BOUND             = "bufferBound";
    public static final String UPLOAD_TIMEOUT           = "uploadTimeout";
    public static final String THRESHOLD                = "threshold";
    public static final String UPLOAD_DIR               = "uploadDir";
    public static final String SSL_CONFIG               = "SSLConfig";
    public static final String DATA_DIR                 = "dataDirectory";
    public static final String EXTRA_DATA_DIR           = "extraDataDirectory";
    public static final String NUM_OF_PEERS             = "peers";
    public static final String CRL_FOLDER               = "CRLFolder";
    /**
     * Max zip entry property in config file
     */
    public static final String MAX_ZIP_ENTRY            = "max_zip_entry";
  }

  /**
   * Constants for the messags from a PublicWBB
   */
  public static class Messages {

    public static final String MSG_TYPE                  = "type";
    public static final String MSG_TYPE_ERROR            = "ERROR";
    public static final String MSG_TYPE_ACCEPTED         = "accepted";
    public static final String MSG_TYPE_ACCEPTED_NO_DATA = "accepted_no_data";
    public static final String MSG                       = "msg";
  }
  public static class OutputFields {
    public static final String COMMIT_TIME                  = "commitTime";
    public static final String JOINT_SIG                  = "jointSig";
    public static final String JSON_FILE                  = "jsonFile";
    public static final String ATTACHMENT_FILE                  = "attachmentFile";
    
  }
}
