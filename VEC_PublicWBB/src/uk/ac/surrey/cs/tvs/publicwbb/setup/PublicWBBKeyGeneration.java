/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb.setup;

import java.io.File;
import java.io.IOException;
import java.security.KeyStore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.KeyGeneration;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateRequestGenException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * User interface used to generate keys on the Public WBB.
 * 
 * @author Chris Culnane
 */
public class PublicWBBKeyGeneration extends KeyGeneration {

  /**
   * Logger
   */
  private static final Logger   logger             = LoggerFactory.getLogger(PublicWBBKeyGeneration.class);

  /**
   * Path to the base keystore.
   */
  protected static final String BASE_KEYSTORE_PATH = "./defaults/basekeystore.jks";

  /**
   * Constructor which requires the identifier for the peer.
   * 
   * @param peerID
   *          The peer identifier.
   */
  public PublicWBBKeyGeneration(String peerID) {
    super(peerID);
  }

  /**
   * Generates SK1, SK2 and SSL keys and the corresponding CSRs for a peer
   * 
   * @param csrFolder
   *          String path to folder to save CSRs to
   * @param keyStorePath
   *          String path to KeyStore location to store keys
   * @throws CertificateRequestGenException
   */
  public void generateKeyAndCSR(String csrFolder, String keyStorePath) throws CertificateRequestGenException {
    try {
      KeyStore ks = CryptoUtils.loadKeyStore(BASE_KEYSTORE_PATH);
      File csrOutput = new File(csrFolder);
      IOUtils.checkAndMakeDirs(csrOutput);
      

      this.generateKey("", new File(csrOutput, this.id + CSR_EXT), ks, SignatureType.RSA);

      File keyStoreFile = new File(keyStorePath);
      IOUtils.checkAndMakeDirs(keyStoreFile.getParentFile());
      
      CryptoUtils.storeKeyStore(ks, keyStorePath);
    }
    catch (CryptoIOException e) {
      throw new CertificateRequestGenException("Exception whilst generating key and CSR", e);
    }
    catch (KeyGenerationException e) {
      throw new CertificateRequestGenException("Exception whilst generating key and CSR", e);
    }
    catch (TVSSignatureException e) {
      throw new CertificateRequestGenException("Exception whilst generating key and CSR", e);
    }
    catch (IOException e) {
      throw new CertificateRequestGenException("Exception whilst creating output directory", e);
    }
  }

  /**
   * Main method for running key generation and importing certificates.
   * 
   * @param args
   *          String array of program arguments
   */
  public static void main(String[] args) {
    CryptoUtils.initProvider();

    if (args.length < 1) {
      printUsage();
      return;
    }

    if (args[0].equals("generate")) {
      if (args.length != 4) {
        printUsage();
      }
      else {
        PublicWBBKeyGeneration kg = new PublicWBBKeyGeneration(args[1]);
        try {
          kg.generateKeyAndCSR(args[2], args[3]);

        }
        catch (CertificateRequestGenException e) {
          logger.error("Error whilst generating keys and CSRs", e);
        }
      }
    }
    else if (args[0].equals("import")) {
      if (args.length != 5) {
        printUsage();
      }
      else {
        try {
          KeyGeneration.importCertificates(args[2], args[3], args[1], args[4], "");

        }
        catch (CertificateStoringException e) {
          logger.error("Error whilst importing certificates");
        }
      }
    }
    else {
      printUsage();
    }
  }

  /**
   * Prints the usage instructions to the System.out
   */
  public static void printUsage() {
    System.out.println("Usage of PublicWBBKeyGeneration:");
    System.out.println("PublicWBBKeyGeneration is used to generate keys, CSRs and");
    System.out.println("import the relevant certificates.");
    System.out.println("Usage:");
    System.out.println("PublicWBBKeyGeneration action [options]");
    System.out.println("PublicWBBKeyGeneration generate entity csrFolder keyStorepath");
    System.out.println("PublicWBBKeyGeneration import entity certFilePath caFilePath keyStorepath");
  }
}
