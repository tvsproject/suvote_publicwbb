/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import java.security.KeyStoreException;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.publicwbb.exceptions.MessageException;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * CommitMessage represents a commit message sent from an MBB Peer to the PublicWBB. Such a message contains a share of a threshold
 * signature of the underlying signature
 * 
 * @author Chris Culnane
 * 
 */
public class CommitMessage {

  /**
   * Path to commit schema file
   */
  private static final String COMMIT_SCHEMA_PATH="commitschema.schema";
  
  /**
   * Commit message type.
   */
  private static final String FINAL_COMMIT_MESSAGE_TYPE = "Commit";
  
  /**
   * JSONObject holding the message
   */
  private JSONObject          msg;

  /**
   * Logger
   */
  private static final Logger logger = LoggerFactory.getLogger(CommitMessage.class);

  /**
   * Construct a new CommitMessage from a String containing a suitable JSONObject
   * 
   * @param msg
   *          String containing JSON of the commit message
   * @throws MessageException
   */
  public CommitMessage(String msg) throws MessageException {
    super();

    try {
      this.msg = new JSONObject(msg);
    }
    catch (JSONException e) {
      throw new MessageException("Exception parsing message", e);
    }
  }

  /**
   * Gets a long of the file size of the commit that is related to this message
   * 
   * @return The attachment size.
   * @throws JSONException
   */
  public long getAttachmentSize() throws JSONException {
    return this.msg.getLong(MessageFields.CommitSK2.ATTACHMENT_SIZE);
  }

  /**
   * Gets the commitTime of the message
   * 
   * @return String containing the commit time this message relates to
   * @throws JSONException
   */
  private String getCommitTime() throws JSONException {
    return this.msg.getString(MessageFields.CommitSK2.COMMIT_TIME);
  }
  public boolean hasCommitDesc(){
    return this.msg.has(MessageFields.CommitSK2.COMMIT_DESC);
  }
  
  public String getCommitDesc() throws JSONException {
    if(this.msg.has(MessageFields.CommitSK2.COMMIT_DESC)){
      return this.msg.getString(MessageFields.CommitSK2.COMMIT_DESC);
    }else{
      return "";
    }
  }

  public String getCommitID() throws JSONException{
    return getCommitTime()+getCommitDesc();
  }
  /**
   * Gets a long of the file size of the commit that is related to this message
   * 
   * @return long value of the fileSize parameter - represents the size of the file
   * @throws JSONException
   */
  public long getFileSize() throws JSONException {
    return this.msg.getLong(MessageFields.CommitSK2.FILE_SIZE);
  }

  /**
   * Gets a string of the peer identifier that this message was received from
   * 
   * @return String of the Peer identifier that send the message
   * @throws JSONException
   */
  public String getFromPeer() throws JSONException {
    return this.msg.getString(MessageFields.FROM_PEER);
  }

  /**
   * Gets a string representation of the hash of the commit that this message signs
   * 
   * @return String of the commit hash
   * @throws JSONException
   */
  public String getHash() throws JSONException {
    return this.msg.getString(MessageFields.CommitSK2.HASH);
  }

  /**
   * Gets the message as a string
   * 
   * @return String of the message
   */
  public String getMsgString() {
    return this.msg.toString();
  }

  /**
   * Performs validation of the incoming message. This consists of a JSON Schema check as well as a signature check
   * 
   * @param server
   *          PublicWBB server that this is being run on
   * @return boolean true if valid, false if invalid. Throws a MessageException if the signature cannot be checked
   * @throws MessageException
   */
  public boolean performValidation(PublicWBB server) throws MessageException {
    // Validate the JSON against the Cancel Schema
    try {
      if (!JSONUtils.validateSchema(JSONUtils.loadSchema(COMMIT_SCHEMA_PATH), this.msg.toString())) {
        logger.warn("JSON Schema validation failed");
        throw new MessageException("JSON Schema validation failed");
      }
      
      TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, server.getCertificateFromCerts(this.msg
          .getString(MessageFields.CommitSK2.PEER_ID) + SystemConstants.SIGNING_KEY_SK2_SUFFIX));
      tvsSig.setPartial(true);
      tvsSig.update(FINAL_COMMIT_MESSAGE_TYPE); 
      tvsSig.update(this.msg.getString(MessageFields.CommitSK2.COMMIT_TIME));
      tvsSig.update(IOUtils.decodeData(EncodingType.BASE64, this.msg.getString(MessageFields.CommitSK2.HASH)));
      if(this.msg.has(MessageFields.CommitSK2.COMMIT_DESC)){
        tvsSig.update(this.msg.getString(MessageFields.CommitSK2.COMMIT_DESC));
      }
      return tvsSig.verify(this.msg.getString(MessageFields.CommitSK2.PEER_SIG), EncodingType.BASE64);
    }
    catch (JSONException e) {
      logger.warn("Error when reading value from JSON Object:{}", this.msg);
      throw new MessageException("Error when reading value from JSON Object", e);
    }
    catch (KeyStoreException e) {
      logger.error("Key Store Exception", e);
      throw new MessageException("Error when initiating signature", e);
    }
    catch (TVSSignatureException e) {
      logger.error("Signature Exception", e);
      throw new MessageException("Error when updating signature", e);
    }
    catch (JSONIOException e) {
      logger.warn("Error when reading value from JSON Schema");
      throw new MessageException("Error when reading value from JSON Schema", e);
    }
  }

  /**
   * Sets which peer this mesage was received from
   * 
   * @param fromPeer
   *          String peer identifier
   * @throws JSONException
   */
  public void setFromPeer(String fromPeer) throws JSONException {
    this.msg.put(MessageFields.FROM_PEER, fromPeer);
  }
}
