/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.net.ssl.SSLSocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CommitMessageListener listens for incoming connection from MBBPeers that will be sending their completed commits.
 * 
 * @author Chris Culnane
 * 
 */
public class CommitMessageListener implements Runnable {

  /**
   * Logger
   */
  private static final Logger logger                   = LoggerFactory.getLogger(CommitMessageListener.class);

  /**
   * Set to true to shutdown the listener
   */
  private boolean             shutdown                 = false;

  /**
   * PublicWBB server this listener is running on
   */
  private PublicWBB           server;

  /**
   * ExecutorService use for incoming messages
   */
  private ExecutorService     externalListenerExecutor = Executors.newFixedThreadPool(64);

  /**
   * Constructs a new CommitMessageListener to listen for connections from MBB peers
   * 
   * @param server
   *          PublicWBB server that this is listening on
   */
  public CommitMessageListener(PublicWBB server) {
    super();

    this.server = server;
  }

  /**
   * Continuously listen for messages, create processor thread and pass to the executor
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    // Loop until shutdown
    while (!this.shutdown) {
      try {
        // Get the external socket from the peer and call accept, which blocks until a connection is received
        SSLSocket socket = (SSLSocket) this.server.getExternalServerSocket().accept();

        // Create CommitMessageThread (it is a Runnable, not a pure thread)
        CommitMessageThread commitMessageThread = new CommitMessageThread(this.server, socket);

        // Add the wbbPeerThread processor the executor
        this.externalListenerExecutor.execute(commitMessageThread);
      }
      catch (IOException e) {
        logger.error("IOException whilst accepting connection on External Server Socket", e);
      }
    }
  }

  /**
   * Shuts down the listener.
   */
  public void shutdown() {
    this.shutdown = true;
  }
}
