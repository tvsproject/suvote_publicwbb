/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.publicwbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.publicwbb.exceptions.DuplicateKeyException;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;

/**
 * Provides a wrapper of the underlying MongoDB that holds the records of the incoming messages.
 * 
 * @author Chris Culnane
 * 
 */
public class DBWrapper {

  /**
   * Logger
   */
  private static final Logger logger         = LoggerFactory.getLogger(DBWrapper.class);

  /**
   * MongoClient to access the underlying database
   */
  private MongoClient         mongoClient;

  /**
   * Mongo DB instance
   */
  private DB                  db;

  /**
   * DBCollection of the commit data
   */
  private DBCollection        coll;

  /**
   * The database name.
   */
  private static final String databaseName   = "PublicWBB";

  /**
   * The commits collection name.
   */
  private static final String collectionName = "Commits";

  /**
   * Constructs a new DBWrapper to access the underlying data
   */
  public DBWrapper() {
    super();

    try {
      this.mongoClient = new MongoClient();
    }
    catch (UnknownHostException e) {
      logger.error("Could not connect to MongoDB", e);
    }
    this.db = this.mongoClient.getDB(databaseName);

    // Get the various collections
    this.coll = this.db.getCollection(collectionName);
    logger.info("Database initialised");
  }

  /**
   * Adds commit data to the set of data received for a particular commit time
   * 
   * @param commitTime
   *          String commit time the data is in relation to
   * @param hash
   *          String hash of data
   * @param filePathJSON
   *          String file path of the actual data file
   * @param filePathAttachments
   *          path to the attachments file
   * @throws AlreadyReceivedMessageException
   */
  public void addCommitData(String commitTime, String hash, String filePathJSON, String filePathAttachments)
      throws AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, commitTime);
    query.append(DBFields.COMMIT_DATA_FILES, new BasicDBObject(DBFields.NOT, new BasicDBObject(DBFields.ELEMENT_MATCH,
        new BasicDBObject(DBFields.DATA_HASH, hash))));

    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();
    data.append(DBFields.DATA_HASH, hash);
    data.append(DBFields.FILE_PATH_JSON, filePathJSON);
    data.append(DBFields.FILE_PATH_ATTACHMENTS, filePathAttachments);

    update.append(DBFields.ADD_TO_SET, new BasicDBObject(DBFields.COMMIT_DATA_FILES, data));

    try {
      WriteResult wr = this.coll.update(query, update, true, false);

      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}");
      }
    }
    catch (MongoException.DuplicateKey e) {
      throw new AlreadyReceivedMessageException("Data has already been stored", e);
    }
  }

  /**
   * Adds a valid commit message to the database (this does not indicate we have a consensus), just that the message itself is
   * valid. Only accepts a message if we haven't already stored a message from this peer.
   * 
   * @param commit
   *          CommitMessage to add to the database
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   */
  public void addValidCommit(CommitMessage commit) throws JSONException, AlreadyReceivedMessageException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, commit.getCommitID());
    //query.append(DBFields.DATA_PATH, new BasicDBObject(DBFields.EXISTS, false));

    // Check we haven't already stored a message from this peer
    query.append(DBFields.VALID_MSGS_RECEIVED, new BasicDBObject(DBFields.NOT, new BasicDBObject(DBFields.ELEMENT_MATCH,
        new BasicDBObject(DBFields.FROM_PEER, commit.getFromPeer()))));

    // Prepare the update
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();
    data.append(DBFields.FROM_PEER, commit.getFromPeer());
    data.append(DBFields.MSG, commit.getMsgString());
    update.append(DBFields.ADD_TO_SET, new BasicDBObject(DBFields.VALID_MSGS_RECEIVED, data));

    try {
      WriteResult wr = this.coll.update(query, update, true, false);

      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}");
      }
    }
    catch (MongoException.DuplicateKey e) {
      throw new AlreadyReceivedMessageException("Message from Peer has already been recorded", e);
    }
  }

  /**
   * Sets the final location of the commit data files once a consensus has been received
   * 
   * @param commitTime
   *          String containing Commit Time we are processing
   * @param jointSignature
   *          String joint Signature of the joint hash
   * @param dataPath
   *          String path to the JSON data file
   * @param dataPathAttachments
   *          String path to the attachments zip file
   * @throws DuplicateKeyException
   */
  public void setFinalCommit(String commitTime, String jointSignature, String dataPath, String dataPathAttachments)
      throws DuplicateKeyException {
    BasicDBObject query = new BasicDBObject(DBFields.ID, commitTime);
    query.append(DBFields.DATA_PATH, new BasicDBObject(DBFields.EXISTS, false));
    BasicDBObject update = new BasicDBObject();
    BasicDBObject data = new BasicDBObject();
    data.append(DBFields.DATA_PATH, dataPath);
    data.append(DBFields.DATA_PATH_ATTACHMENTS, dataPathAttachments);
    data.append(DBFields.JOINT_SIGNATURE, jointSignature);
    update.append(DBFields.SET, data);
    try {
      WriteResult wr = this.coll.update(query, update, true, false);
      if (wr.getN() == 0) {
        logger.debug("Document NOT Updated:{}");
      }
    }
    catch (MongoException.DuplicateKey e) {
      throw new DuplicateKeyException("Message has already been recorded", e);
    }
  }

  /**
   * Gets the messages that have been stored (which are all individually valid) for a particular commit time
   * 
   * @param commitTime
   *          String commit time to get messages for
   * @return List<JSONObject> of individually valid messages received
   * @throws JSONException
   */
  public List<JSONObject> getValidReceivedMessages(String commitTime) throws JSONException {
    ArrayList<JSONObject> validMessages = new ArrayList<JSONObject>();
    BasicDBObject query = new BasicDBObject(DBFields.ID, commitTime);

    BasicDBObject found = (BasicDBObject) this.coll.findOne(query);

    if (found != null) {
      BasicDBList list = (BasicDBList) found.get(DBFields.VALID_MSGS_RECEIVED);
      Iterator<Object> itr = list.iterator();

      while (itr.hasNext()) {
        validMessages.add(new JSONObject(((BasicDBObject) itr.next()).getString(DBFields.MSG)));
      }
    }

    return validMessages;
  }

  /**
   * Gets the path to the data files associated with a particular commitTime and hash. Their could be multiple instances of the
   * files if two peers sent the same file at the same time. As such, this method gets the list of all received files for this
   * CommitTime and returns the first matching entry. An entry will only have been made if the sender was sending a valid commit, so
   * we can be assured these submissions are valid, but they might not all be the same.
   * 
   * @param commitTime
   *          String containing the commitTime being processed
   * @param hash
   *          String containing the hash to get the data files for
   * @return String array with two values 0-the path to the JSON and 1-is the path to the attachment file, or null if no match is
   *         found
   */
  public String[] getCommitDataPath(String commitTime, String hash) {
    // Gets a matching record for the commitTime where COMMIT_DATA_FILES contains the required hash
    BasicDBObject query = new BasicDBObject(DBFields.ID, commitTime);
    query
        .append(DBFields.COMMIT_DATA_FILES, new BasicDBObject(DBFields.ELEMENT_MATCH, new BasicDBObject(DBFields.DATA_HASH, hash)));
    BasicDBObject found = (BasicDBObject) this.coll.findOne(query);
    // If we didn't find any we return null
    if (found == null) {
      return null;
    }
    else {
      // We now get the list of all data files - this will contain both matching and non-matching hashes. They query was that the
      // sub-document contained the hash, but it doesn't do a partial return on the sub-document, so we still need to manually find
      // it in the set of sub-documents
      BasicDBList list = (BasicDBList) found.get(DBFields.COMMIT_DATA_FILES);
      for (int i = 0; i < list.size(); i++) {
        BasicDBObject listEntry = (BasicDBObject) list.get(i);
        if (listEntry.containsField(DBFields.DATA_HASH) && listEntry.getString(DBFields.DATA_HASH).equals(hash)) {
          // If we have a DATA_HASH field and it matches return a String array with [0] being the path to the JSON data and [1]
          // being the path to the zip attachment
          String[] result = new String[2];
          result[0] = listEntry.getString(DBFields.FILE_PATH_JSON);
          result[1] = listEntry.getString(DBFields.FILE_PATH_ATTACHMENTS);
          return result;
        }
      }
      return null;

    }
  }

  /**
   * Checks whether we have already stored commit data for this commitTime and hash.
   * 
   * @param commitTimeDesc
   *          String commit time to check (concatenated with a description if it exists)
   * @param hash
   *          String hash of data
   * @return boolean if we already have a record of that data, false if we do not
   */
  public boolean haveCommitData(String commitTimeDesc, String hash) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, commitTimeDesc);
    query
        .append(DBFields.COMMIT_DATA_FILES, new BasicDBObject(DBFields.ELEMENT_MATCH, new BasicDBObject(DBFields.DATA_HASH, hash)));
    BasicDBObject found = (BasicDBObject) this.coll.findOne(query);

    if (found == null) {
      return false;
    }
    else {
      return true;
    }
  }

  /**
   * Checks whether the specified commitTime has already been finalised. A commit is finalised when a threshold of consistent
   * commits is received and combined
   * 
   * @param commitTime
   *          String containing the commitTime to check
   * @return boolean true if this commit time has been finalised, otherwise false
   */
  public boolean hasBeenFinalised(String commitTime) {
    BasicDBObject query = new BasicDBObject(DBFields.ID, commitTime);
    query.append(DBFields.DATA_PATH, new BasicDBObject(DBFields.EXISTS, true));

    BasicDBObject found = (BasicDBObject) this.coll.findOne(query);

    if (found == null) {
      return false;
    }
    else {
      return true;
    }
  }
}
