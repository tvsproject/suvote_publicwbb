/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.security.cert.CRLException;
import java.security.cert.CertificateException;
import java.security.cert.CollectionCertStoreParameters;
import java.security.cert.PKIXBuilderParameters;
import java.security.cert.X509CertSelector;

import javax.net.ServerSocketFactory;
import javax.net.ssl.CertPathTrustManagerParameters;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.TrustManagerFactory;

import org.bouncycastle.asn1.x500.RDN;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.IETFUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.publicwbb.exceptions.ServerInitException;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSCertificate;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.tvs.cs.utils.crls.CachedCRLManager;
import uk.ac.surrey.tvs.cs.utils.fswatcher.FileWatcherException;

/**
 * @author Chris Culnane
 * 
 */
public class PublicWBB {

  /**
   * Logger
   */
  private static final Logger   logger                 = LoggerFactory.getLogger(PublicWBB.class);

  /**
   * SSL context for internal communication
   */
  private SSLContext            ctx;

  /**
   * TrustManager for internal communication
   */
  private TrustManagerFactory   tmf;

  /**
   * A keystore for all the certificates
   */
  private TVSKeyStore           certs;

  /**
   * KeyManager factory for internal communication
   */
  private KeyManagerFactory     kmf;

  /**
   * The peer configuration
   */
  private ConfigFile            config;

  /**
   * SSLServerFactory for creating SSLServerSockets for receiving connections from other peers
   */
  private ServerSocketFactory   sslSocketFactory;

  /**
   * The SSL socket in use.
   */
  private SSLServerSocket       serverSocket;

  /**
   * Thread used to listen for requests.
   */
  private Thread                listenerThread;

  /**
   * Listener for commit messages.
   */
  private CommitMessageListener listener;

  /**
   * Database for storing commits.
   */
  private DBWrapper             db;

  /**
   * Directory used to store uploaded file from messages.
   */
  private File                  uploadDir;

  /**
   * CachedCRLManager maintains a cache of current CRLs and dynamically reloads changed files
   */
  private CachedCRLManager      crlManager             = null;

  /**
   * value in the config file that indicates the SSL should use the default cipher suite
   */
  private static final String   DEFAULT_SSL_PARAM      = "default";

  /**
   * Static string that contains the location of the config file schema
   */
  private static final String   PUBLIC_WBB_SCHEMA_FILE = "./publicwbbconfigschema.schema";

  /**
   * Constructor requiring the configuration file.
   * 
   * @param configFile
   *          The peer configuration.
   * @throws JSONIOException
   * @throws ServerInitException
   * @throws IOException 
   */
  public PublicWBB(String configFile) throws JSONIOException, ServerInitException, IOException {
    super();

    logger.info("Starting PublicWBB with configFile: {}", configFile);
    this.config = new ConfigFile(configFile, PUBLIC_WBB_SCHEMA_FILE);
    this.db = new DBWrapper();
    this.uploadDir = new File(this.config.getStringParameter(PublicWBBConstants.Config.UPLOAD_DIR));
    IOUtils.checkAndMakeDirs(this.uploadDir);
    

    this.init();
    logger.info("Finished loading PublicWBB");
  }

  /**
   * Gets a certificate given an alias.
   * 
   * @param alias
   *          The alias used to obtain the certificate.
   * @return The retrieved certificate, if one exists.
   * @throws KeyStoreException
   */
  public TVSCertificate getCertificateFromCerts(String alias) throws KeyStoreException {
    return this.certs.getCertificate(alias);
  }

  /**
   * Gets the name of a certificate for an X500 principal.
   * 
   * @param principal
   *          The X500 principal.
   * @return The certificate name.
   */
  public String getCertificateName(Principal principal) {
    X500Name certName = new X500Name(principal.getName());
    RDN cn = certName.getRDNs(BCStyle.CN)[0];

    return IETFUtils.valueToString(cn.getFirst().getValue());
  }

  /**
   * @return The peer config.
   */
  public ConfigFile getConfig() {
    return this.config;
  }

  /**
   * @return The concurrent database wrapper.
   */
  public DBWrapper getDB() {
    return this.db;
  }

  /**
   * @return The SSL server socket in use.
   */
  public SSLServerSocket getExternalServerSocket() {
    return this.serverSocket;
  }

  /**
   * @return The directory used to store uploaded file from messages.
   */
  public File getUploadDir() {
    return this.uploadDir;
  }

  /**
   * Initialises the peer.
   * 
   * @throws ServerInitException
   */
  private void init() throws ServerInitException {
    try {
      logger.info("Initialising PublicWBB");
      CryptoUtils.initProvider();
      this.crlManager = new CachedCRLManager(this.config.getStringParameter(PublicWBBConstants.Config.CRL_FOLDER));
      this.crlManager.startWatching();

      this.certs = TVSKeyStore.getInstance("JKS");
      this.certs.load(this.config.getStringParameter(PublicWBBConstants.Config.CERTIFICATE_KEYSTORE), this.config
          .getStringParameter(PublicWBBConstants.Config.CERTIFICATE_KEYSTORE_PWD).toCharArray());
      logger.info("Loaded Keystore");

      this.initTrustStore(this.config.getStringParameter(PublicWBBConstants.Config.KEYSTORE_PATH),
          this.config.getStringParameter(PublicWBBConstants.Config.KEYSTORE_PWD).toCharArray());
      logger.info("Create Truststore");

      int port = this.config.getIntParameter(PublicWBBConstants.Config.LISTEN_PORT);
      this.ctx = SSLContext.getInstance("TLS");

      this.ctx.init(this.kmf.getKeyManagers(), this.tmf.getTrustManagers(), null);
      this.sslSocketFactory = this.ctx.getServerSocketFactory();
      logger.info("Created context and SocketFactory");

      this.serverSocket = (SSLServerSocket) this.sslSocketFactory.createServerSocket();
      this.serverSocket.setNeedClientAuth(true);
      this.serverSocket.setUseClientMode(false);
      this.serverSocket.setReuseAddress(true);

      if (!this.config.getStringParameter(PublicWBBConstants.Config.SSL_CONFIG).equalsIgnoreCase(DEFAULT_SSL_PARAM)) {
        logger.info("Setting enabled cipher suites to {}", this.config.getStringParameter(PublicWBBConstants.Config.SSL_CONFIG));
        this.serverSocket.setEnabledCipherSuites(new String[] { this.config
            .getStringParameter(PublicWBBConstants.Config.SSL_CONFIG) });
      }

      InetSocketAddress sa = new InetSocketAddress(port);
      this.serverSocket.bind(sa);
      logger.info("Bound to socket address {}", sa);

      this.listener = new CommitMessageListener(this);
    }
    catch (IOException | KeyManagementException | NoSuchAlgorithmException | KeyStoreException | CertificateException
        | CRLException | FileWatcherException e) {
      logger.error("Exception whilst starting init", e);
      throw new ServerInitException("Exception whilst initialising the server", e);
    }
  }

  /**
   * Initialises the key store given the store's password.
   * 
   * @param pathToKS
   *          The key store location.
   * @param pwd
   *          The key store password.
   * @throws PeerSSLInitException
   */
  private void initTrustStore(String pathToKS, char[] pwd) throws ServerInitException {
    FileInputStream fis = null;
    logger.info("Initialising Truststore");

    try {
      KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
      fis = new java.io.FileInputStream(pathToKS);
      ks.load(fis, pwd);

      this.tmf = TrustManagerFactory.getInstance("PKIX");
      PKIXBuilderParameters pkixParams = new PKIXBuilderParameters(ks, new X509CertSelector());
      java.security.cert.CertStore cs = java.security.cert.CertStore.getInstance("Collection", new CollectionCertStoreParameters(
          crlManager.getCRLCollection()));
      pkixParams.addCertStore(cs);
      tmf.init(new CertPathTrustManagerParameters(pkixParams));

      this.kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
      this.kmf.init(ks, pwd);
      logger.info("Finished loading truststore");
    }
    catch (Exception e) {
      logger.error("Error whilst initialising SSL Key and Trust Stores", e);
      throw new ServerInitException("Error whilst initialising SSL Key and Trust Stores", e);
    }
    finally {
      if (fis != null) {
        try {
          fis.close();
        }
        catch (IOException ie) {
          logger.error("Error whilst initialising SSL Key and Trust Stores (Closing File)", ie);
          throw new ServerInitException("Error whilst initialising SSL Key and Trust Stores (Closing File)", ie);
        }
      }
    }
  }

  /**
   * Sends an error message to the client.
   * 
   * @param error
   *          The error to send.
   * @param sock
   *          The connected socket for the client.
   */
  public void sendErrorMessage(PublicWBBCommitError error, Socket sock) {
    try {
      JSONObject response = new JSONObject();
      response.put(PublicWBBConstants.Messages.MSG_TYPE, PublicWBBConstants.Messages.MSG_TYPE_ERROR);

      switch (error) {
        case DUPLICATE_ENTRY:
          response.put(PublicWBBConstants.Messages.MSG,
              "Public commit already received from peer");
          break;
        case FAILED_VALIDATION:
          response.put(PublicWBBConstants.Messages.MSG, "Message failed validation");
          break;
        case SIGNATURE_FAILURE:
          response.put(PublicWBBConstants.Messages.MSG, "Message Signature failed");
          break;
        case INVALID_FILE:
          response.put(PublicWBBConstants.Messages.MSG, "Submitted file does not match submitted hash");
          break;
        case INVALID_MESSAGE:
          response.put(PublicWBBConstants.Messages.MSG, "Message cannot be parsed");
          break;
        default:
          response.put(PublicWBBConstants.Messages.MSG, "Unknown error encountered");
          break;
      }

      this.sendMessageAndClose(response.toString(), sock);
    }
    catch (IOException | JSONException e) {
      logger.error("Exception whilst trying to send error message", e);
    }
  }

  /**
   * Utility method to send an error message and close the socket connection.
   * 
   * @param message
   *          the message to send
   * @param sock
   *          the socket to send the message on
   * @throws IOException
   */
  public void sendMessageAndClose(String message, Socket sock) throws IOException {
    logger.info("Sending Response: {} to {}",message,sock);
    PrintWriter pw = new PrintWriter(sock.getOutputStream(), true);
    pw.println(message);
    pw.close();
    sock.close();
  }

  /**
   * Sends a response message to the client.
   * 
   * @param responseType
   *          The response to send.
   * @param sock
   *          The socket for the client.
   */
  public void sendResponseMessage(PublicWBBCommitResponse responseType, Socket sock) {
    try {
      JSONObject response = new JSONObject();

      switch (responseType) {
        case ACCEPTED:
          response.put(PublicWBBConstants.Messages.MSG_TYPE, PublicWBBConstants.Messages.MSG_TYPE_ACCEPTED);
          response.put(PublicWBBConstants.Messages.MSG, "Message accepted and data downloaded.");
          break;
        case ACCEPTED_DATA_RECEIVED_ALREADY:
          response.put(PublicWBBConstants.Messages.MSG_TYPE, PublicWBBConstants.Messages.MSG_TYPE_ACCEPTED_NO_DATA);
          response.put(PublicWBBConstants.Messages.MSG, "Message accepted, data already downloaded from another peer.");
          break;
        default:
          logger.error("Unknown response type submitted, cannot send message");
      }

      this.sendMessageAndClose(response.toString(), sock);
    }
    catch (IOException | JSONException e) {
      logger.error("Exception whilst sending response", e);
    }
  }

  /**
   * Starts listening for messages via a thread.
   */
  public void startListening() {
    this.listenerThread = new Thread(this.listener, "PublicWBB-ExternalListener");
    this.listenerThread.start();
    logger.info("PublicWBB started listening");
  }

  /**
   * Main entry point for the peer. Starts everything running.
   * 
   * @param args
   *          None expected.
   * @throws IOException 
   */
  public static void main(String[] args) throws JSONIOException, ServerInitException, IOException {
    if (args.length != 1) {
      printUsage();
    }
    else {
      PublicWBB publicWBB = new PublicWBB(args[0]);
      publicWBB.startListening();
    }
  }

  /**
   * Prints the usage instructions for starting the UI
   */
  private static final void printUsage() {
    System.out.println("Usage of PublicWBB:");
    System.out.println("PublicWBB starts the Public WBB receiver service. This");
    System.out.println("listens for connections from peers and processes their");
    System.out.println("daily commit packages");
    System.out.println("Usage:\n");
    System.out.println("PublicWBB [pathToConfigFile] \n");
    System.out.println("\tpathToConfigFile: path to configuration file to use.");
  }
}
