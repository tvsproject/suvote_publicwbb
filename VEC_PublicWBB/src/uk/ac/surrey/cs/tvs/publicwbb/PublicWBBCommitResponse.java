/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

/**
 * Enum that specifies the different types of responses the PublicWBB can send <br>
 * ACCEPTED - indicates message and data received and accepted <br>
 * ACCEPTED_DATA_RECEIVED_ALREADY - indicates the message has been received and accepted, but the data will be ignored because we
 * have already downloaded it from another peer
 * 
 * @author Chris Culnane
 * 
 */
public enum PublicWBBCommitResponse {
  ACCEPTED, ACCEPTED_DATA_RECEIVED_ALREADY
}
