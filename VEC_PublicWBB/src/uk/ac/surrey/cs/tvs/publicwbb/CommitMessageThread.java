/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.SocketException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.SSLSocket;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.CommitSK2;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.publicwbb.PublicWBBConstants.Config;
import uk.ac.surrey.cs.tvs.publicwbb.PublicWBBConstants.OutputFields;
import uk.ac.surrey.cs.tvs.publicwbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.publicwbb.exceptions.DuplicateKeyException;
import uk.ac.surrey.cs.tvs.publicwbb.exceptions.MessageException;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSCertificate;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSCombiner;
import uk.ac.surrey.cs.tvs.utils.io.BoundedBufferedInputStream;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.ZipUtil;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.StreamBoundExceededException;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileEntryTooBigException;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.UploadedZipFileNameException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.messages.FileMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.wbb.messages.Message;

/**
 * Processes incoming connections from the CommitMessageListener
 * 
 * Reads the message from the stream, verifies and processes the contents.
 * 
 * @author Chris Culnane
 * 
 */
public class CommitMessageThread implements Runnable {

  /**
   * Socket from the accepted connection
   */
  private SSLSocket                  socket;

  /**
   * The BoundedBufferInputStream we will use to read from the socket
   */
  private BoundedBufferedInputStream in;

  /**
   * The underlying PublicWBB server
   */
  private PublicWBB                  server;

  /**
   * Logger
   */
  private static final Logger        logger                          = LoggerFactory.getLogger(CommitMessageThread.class);

  /**
   * Prefix to use when creating temp attachment folders
   */
  public static final String         ATTACHMENT_FOLDER_PREFIX        = "attachments";

  /**
   * Prefix to use for Commit Upload files (both data and attachments
   */
  public static final String         COMMIT_UPLOAD_PREFIX            = "commitUpload";

  /**
   * Suffix for JSON data upload files
   */
  public static final String         COMMIT_UPLOAD_SUFFIX            = "commit.json";

  /**
   * Suffix for attachment zip upload files
   */
  public static final String         COMMIT_UPLOAD_ATTACHMENT_SUFFIX = "commitAttachments.zip";

  /**
   * Extension to use on final commit JSON data file
   */
  public static final String         FINAL_COMMIT_EXT                = ".json";
  /**
   * Extension to use on final commit JSON Signature file
   */
  public static final String         FINAL_COMMIT_SIGNATURE_EXT      = "_signature.json";

  /**
   * Extension to use on final commit attachment file
   */
  public static final String         FINAL_COMMIT_ATTACHMENT_EXT     = "_attachments.zip";

  /**
   * Creates a new CommitMessageThread that will process the incoming Commit Messages
   * 
   * @param server
   *          PublicWBB server that this is running on
   * @param socket
   *          SSLSocket from the incoming connection
   * @throws IOException
   */
  public CommitMessageThread(PublicWBB server, SSLSocket socket) throws IOException {
    super();

    this.socket = socket;

    // Setup new BoundedBufferInputStream with bufferCount from config file
    this.in = new BoundedBufferedInputStream(socket.getInputStream(), server.getConfig().getIntParameter(Config.BUFFER_BOUND));
    this.server = server;
  }

  /**
   * Process a single external message.
   * 
   * @see java.lang.Runnable#run()
   */
  @Override
  public void run() {
    logger.debug("Connection from:{}", this.socket.getRemoteSocketAddress());

    String line = "";

    try {
      // Wait until line is ready to be read
      line = this.in.readLine();

      if (line == null) {
        // Catch if the connection has dropped
        logger.info("readLine returned Null - likely connection drop");
        return;
      }

      // Prepare variable to store the incoming message
      CommitMessage commit = null;

      // Try to parse the incoming message
      commit = new CommitMessage(line);
      commit.setFromPeer(this.server.getCertificateName((this.socket.getSession().getPeerPrincipal())));

      // Validate the commit message
      if (!commit.performValidation(this.server)) {
        logger.warn("Message failed validation - rejected:{}", line);
        this.server.sendErrorMessage(PublicWBBCommitError.FAILED_VALIDATION, this.socket);
        return;
      }

      // If we get here the message is valid - check whether we have already received the data associated with this hash. If we have
      // we send a response and close the socket, there is no point downloading the data twice
      if (this.server.getDB().haveCommitData(commit.getCommitID(), commit.getHash())) {
        logger.info("Already received data matching that hash, will not download again");
        this.server.getDB().addValidCommit(commit);
        this.server.sendResponseMessage(PublicWBBCommitResponse.ACCEPTED_DATA_RECEIVED_ALREADY, this.socket);
        this.checkConsensus(commit.getCommitID(), commit.hasCommitDesc());
      }
      else {
        // We haven't got the data, or are still in the process of downloading the data from another peer, so we will start
        // downloading from the peer that sent this message as well

        // Prepare a message digest for validating the received data file
        MessageDigest md = MessageDigest.getInstance(SystemConstants.DEFAULT_MESSAGE_DIGEST);
        IOUtils.checkAndMakeDirs(this.server.getUploadDir());

        // Create output files
        File dest = File.createTempFile(COMMIT_UPLOAD_PREFIX, COMMIT_UPLOAD_SUFFIX, this.server.getUploadDir());
        File destAttachments = File.createTempFile(COMMIT_UPLOAD_PREFIX, COMMIT_UPLOAD_ATTACHMENT_SUFFIX,
            this.server.getUploadDir());

        logger.info("About to read file from stream and save to {} and {}", dest.getAbsolutePath(),
            destAttachments.getAbsolutePath());

        this.socket.setSoTimeout(this.server.getConfig().getIntParameter(Config.UPLOAD_TIMEOUT));

        // Read the file from the stream
        this.in.readFile(commit.getFileSize(), dest);
        this.in.readFile(commit.getAttachmentSize(), destAttachments);

        // Prepare a folder for unzipping the attachments
        Path commitUploadDir = Paths.get(this.server.getUploadDir().getAbsolutePath());
        Path safeUploadDir = Files.createTempDirectory(commitUploadDir, ATTACHMENT_FOLDER_PREFIX);
        ZipUtil zipUtil = new ZipUtil(this.server.getConfig().getIntParameter(Config.MAX_ZIP_ENTRY));
        File safeUploadDirFile = safeUploadDir.toFile();

        try {
          // This performs a safe unzipping of the file. See comments in ZipUtil for more details
          zipUtil.unzip(destAttachments.getAbsolutePath(), safeUploadDirFile);
        }
        catch (UploadedZipFileEntryTooBigException e) {
          logger.error("The Zip File {} entry has exceeded the limit - unzipping abandoned", destAttachments.getAbsolutePath(), e);
          this.socket.setSoTimeout(0);
          this.server.sendErrorMessage(PublicWBBCommitError.INVALID_FILE, this.socket);
        }
        catch (UploadedZipFileNameException e) {
          logger.error("The Zip File {} entry has an invalid filename - unzipping abandoned", destAttachments.getAbsolutePath(), e);
          this.socket.setSoTimeout(0);
          this.server.sendErrorMessage(PublicWBBCommitError.INVALID_FILE, this.socket);
        }

        // Prepare to read the JSON data file and any referenced attachments
        BufferedReader br = null;
        try {
          br = new BufferedReader(new FileReader(dest));

          String jline = "";

          while ((jline = br.readLine()) != null) {
            try {
              // All messages will have a JSONWBBMessage entry
              JSONWBBMessage msg = JSONWBBMessage.parseMessage(jline);
              md.update(msg.getInternalSignableContent().getBytes());

              // If the message contained attachments add them in as well
              if (msg.getType() == Message.FILE_MESSAGE || msg.getType() == Message.BALLOT_AUDIT_COMMIT
                  || msg.getType() == Message.BALLOT_GEN_COMMIT || msg.getType() == Message.MIX_RANDOM_COMMIT) {
                IOUtils.addFileIntoDigest(new File(safeUploadDirFile, ((FileMessage) msg).getFilename()), md);
              }
            }
            catch (MessageJSONException e) {
              logger.error("A message in the commit has caused an Exception", e);
            }
          }
        }
        finally {
          if (br != null) {
            br.close();
          }
        }

        // Get the hash value
        String hash = IOUtils.encodeData(EncodingType.BASE64, md.digest());

        // Check it is equal to the signed message we just received
        if (hash.equals(commit.getHash())) {
          try {
            // Add this as valid commit
            this.server.getDB().addValidCommit(commit);
            // Along with the path to the data files
            try {
              this.server.getDB().addCommitData(commit.getCommitID(), hash, dest.getAbsolutePath(),
                  destAttachments.getAbsolutePath());
              // Response to the sender
              this.server.sendResponseMessage(PublicWBBCommitResponse.ACCEPTED, this.socket);
            }
            catch (AlreadyReceivedMessageException e) {
              logger.info("Already received data matching that hash, will not download again");
              this.server.sendResponseMessage(PublicWBBCommitResponse.ACCEPTED_DATA_RECEIVED_ALREADY, this.socket);
            }

          }
          catch (AlreadyReceivedMessageException e) {
            logger.info("Message from peer has already been received");
            this.server.sendErrorMessage(PublicWBBCommitError.DUPLICATE_ENTRY, this.socket);
          }
          this.checkConsensus(commit.getCommitID(), commit.hasCommitDesc());
        }
        else {
          // The file did not match the hash. It is invalid
          this.server.sendErrorMessage(PublicWBBCommitError.INVALID_FILE, this.socket);
        }
      }

    }
    catch (MessageException e) {
      // The failure could be based on a signature exception
      logger.warn("Message failed validation:{} - rejected:{}", e.getMessage(), line);
      this.server.sendErrorMessage(PublicWBBCommitError.FAILED_VALIDATION, this.socket);
      return;
    }
    catch (JSONException e) {
      logger.warn("Could not parse JSON message:{}, error:{}", line, e.getMessage());
      this.server.sendErrorMessage(PublicWBBCommitError.INVALID_MESSAGE, this.socket);
      return;
    }
    catch (AlreadyReceivedMessageException e) {
      logger.warn("Already received message or Commit already at threshold {}", line);
      this.server.sendErrorMessage(PublicWBBCommitError.DUPLICATE_ENTRY, this.socket);
    }
    catch (NoSuchAlgorithmException e) {
      logger.error("Exception whilst reading file", e);
    }
    catch (SocketException e) {
      logger.info("Socket has closed");
    }
    catch (IOException e) {
      logger.error("IO exception occurred", e);
    }
    catch (StreamBoundExceededException e) {
      logger.warn("Input has exceeded bound on server socket - no newline character found. The data has been discarded.",
          this.socket.getRemoteSocketAddress(), e);
      try {
        this.server.sendMessageAndClose("Input exceed bound", this.socket);
      }
      catch (IOException e1) {
        logger.warn("Exception whilst trying to send error message to client", e);
      }
    }
    finally {
      try {

        this.socket.close();
      }
      catch (IOException e) {
        logger.error("Exception whilst closing socket", e);
      }
    }
  }

  /**
   * Complete the commit by selecting a valid file and copying it to the final destination. This signifies the end of the commit run
   * for the specified commitTime. It also sets the data in the database to indicate the location of the final commit. The copy will
   * not overwrite, instead throwing an exception. Since the final filename is based on the commitTime there should be no situation
   * where duplicate files exist.
   * 
   * @param commitTime
   *          String of the commitTime we are processing
   * @param hash
   *          String hash of the joint commit
   * @param jointSig
   *          String containing the joint signature of the hash
   */
  private void completeCommit(String commitTime, String hash, String jointSig, boolean isExtraCommit) {
    try {
      // Gets the files associated with this commitTime and Hash. The array contains the JSON commit and the attachment zip
      String[] files = this.server.getDB().getCommitDataPath(commitTime, hash);
      // get the JSON Commit
      File jsonFile = new File(files[0]);

      // Get the attachment
      File attachmentsFile = new File(files[1]);

      

      // Create the output directory if it doesn't exist
      File outputDir;
      if (isExtraCommit) {
        outputDir = new File(this.server.getConfig().getStringParameter(PublicWBBConstants.Config.EXTRA_DATA_DIR));
      }
      else {
        outputDir = new File(this.server.getConfig().getStringParameter(PublicWBBConstants.Config.DATA_DIR));
      }
      IOUtils.checkAndMakeDirs(outputDir);

      // Check we haven't already finalised this commit
      if (!this.server.getDB().hasBeenFinalised(commitTime)) {
        logger.info("Final commit files will be copied from {} and {}", jsonFile, attachmentsFile);
        // If the file already exists an exception will be thrown
        // Prepare the final filename, based on the commitTime
        File outputJson = new File(outputDir, commitTime + FINAL_COMMIT_EXT);
        File outputAtt = new File(outputDir, commitTime + FINAL_COMMIT_ATTACHMENT_EXT);
        File outputSig = new File(outputDir, commitTime + FINAL_COMMIT_SIGNATURE_EXT);
        JSONObject sigOutput = new JSONObject();
        try {
          // Set the final commit data in the database
          this.server.getDB().setFinalCommit(commitTime, jointSig, outputJson.getAbsolutePath(), outputAtt.getAbsolutePath());

          sigOutput.put(OutputFields.COMMIT_TIME, commitTime);
          sigOutput.put(OutputFields.JOINT_SIG, jointSig);
          sigOutput.put(OutputFields.JSON_FILE, outputJson.getName());
          sigOutput.put(OutputFields.ATTACHMENT_FILE, outputAtt.getName());
        }
        catch (DuplicateKeyException e) {
          logger.info("Final Commit has already been added");
        }

        // Copy the files, this will throw an exception if the file already exists
        Files.copy(jsonFile.toPath(), outputJson.toPath());
        Files.copy(attachmentsFile.toPath(), outputAtt.toPath());
        IOUtils.writeJSONToFile(sigOutput, outputSig.getAbsolutePath());
        logger.info("Final files outputted to {} and {} and {}", outputJson, outputAtt, outputSig);
      }else{
        logger.info("Final commit has already been finalised");
      }

    }
    catch (FileAlreadyExistsException e) {
      logger.info("Another thread has already created the final commit files, cannot overwrite them");
    }
    catch (IOException e) {
      logger.error("Exception whilst completing commit", e);
    }
    catch (JSONException e) {
      logger.error("Exception whilst storing joint signature data", e);
    }
    catch (JSONIOException e) {
      logger.error("Exception whilst storing joint signature data", e);
    }

  }

  /**
   * Checks whether we have reached a consensus and if we have closes the commit. Also raises an error if we cannot reach a
   * consensus, which shouldn't happen during the normal operation, but if it does happen it requires human intervention to find out
   * what has gone on. It indicates that the threshold assumption has failed.
   * 
   * @param commitTime
   *          String containing the commitTime we are checking
   */
  private void checkConsensus(String commitTime, boolean isExtraCommit) {
    try {
      // Check the number of valid commit messages we have on this CommitTime
      List<JSONObject> validCommitMsgs = this.server.getDB().getValidReceivedMessages(commitTime);
      // Prepare a hashmap for checking we have a threshold on the same hash
      HashMap<String, List<JSONObject>> commitHashMap = new HashMap<String, List<JSONObject>>();
      int threshold = this.server.getConfig().getIntParameter(PublicWBBConstants.Config.THRESHOLD);
      int maxfound = 0;
      boolean completed = false;
      // iterate through each valid commit message and get the hash value
      for (JSONObject validMsg : validCommitMsgs) {
        String msghash = validMsg.getString(MessageFields.CommitSK2.HASH);
        if (commitHashMap.containsKey(msghash)) {
          // If we have already seen that hash increment the counter by adding it to the array in the HashMap.
          commitHashMap.get(msghash).add(validMsg);

          // If we have a threshold we can stop and combine the signatures to form a final joint signature
          if (commitHashMap.get(msghash).size() >= threshold) {
            logger.info("Have a threshold of CommitSK2 on the hash:{}. Will finalise commit", msghash);
            // Prepare the combiner
            int peerCount = this.server.getConfig().getIntParameter(PublicWBBConstants.Config.NUM_OF_PEERS);
            BLSCombiner combiner = new BLSCombiner(peerCount, threshold);
            List<JSONObject> validMessages = commitHashMap.get(msghash);
            for (JSONObject sigMsg : validMessages) {
              TVSCertificate cert = server.getCertificateFromCerts(sigMsg.getString(MessageFields.CommitSK2.PEER_ID)
                  + SystemConstants.SIGNING_KEY_SK2_SUFFIX);
              combiner.addShare(IOUtils.decodeData(EncodingType.BASE64, sigMsg.getString(CommitSK2.PEER_SIG)), cert
                  .getBLSPublicKey().getSequenceNo());
            }
            // Send the combined signature to the completeCommit Method
            completeCommit(commitTime, msghash, IOUtils.encodeData(EncodingType.BASE64, combiner.combined().toBytes()),
                isExtraCommit);
            completed = true;

          }
        }
        else {
          // We haven't seen the hash before, prepare an ArrayList to store any matching hashes in
          ArrayList<JSONObject> msgList = new ArrayList<JSONObject>();
          msgList.add(validMsg);
          commitHashMap.put(msghash, msgList);
        }
        if (commitHashMap.get(msghash).size() > maxfound) {
          maxfound = commitHashMap.get(msghash).size();
        }
      }
      // If we have completed we know we have reached consensus, so don't need to check
      if (!completed) {
        // get the peer count
        int peerCount = this.server.getConfig().getIntParameter(PublicWBBConstants.Config.NUM_OF_PEERS);
        // Find out how many peers are still to send in a submission
        int remaining = peerCount - validCommitMsgs.size();
        // if that is more than we need to reach the threshold we might still reach consensus
        if ((threshold - maxfound) <= remaining) {
          logger.info("Consensus not met but can still be met with more submissions");
        }
        else {
          // if we get here we know we will not reach consensus even if all the remaining peers submit the same hash as the most
          // popular hash currently held
          logger.error("Consensus cannot be met - this requires human intervention");
        }
      }
    }
    catch (JSONException | TVSSignatureException | KeyStoreException e) {
      logger.error("Exception whilst checking commit consensus", e);
    }
  }

}
