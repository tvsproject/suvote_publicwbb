/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;
import java.util.zip.ZipOutputStream;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import javax.security.auth.x500.X500Principal;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.publicwbb.exceptions.ServerInitException;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.ZipUtil;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.wbb.exceptions.MessageJSONException;
import uk.ac.surrey.cs.tvs.wbb.messages.FileMessage;

import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 * This class defines common test parameters and static initialisers within a singleton.
 */
public class TestParameters {

  /** The upload folder. */
  public static final String        UPLOAD_FOLDER             = "upload";

  /** The commits collection in the database. */
  public static final String        COMMITS                   = "Commits";

  /** Test JSON file path. */
  public static final String        JSON_PATH                 = "json_path.json";

  /** Test attachment file path. */
  public static final String        ATTACHMENT_PATH           = "attachment_path.zip";

  /** Commit message type - should really match the CommitProcessor.FINAL_COMMIT_MESSAGE_TYPE private constant. */
  public final static String        COMMIT_MESSAGE_TYPE       = "Commit";

  /** Test commit time. */
  public static final String        COMMIT_TIME               = "1234567890";

  /** Test attachment size. */
  public static final long          ATTACHMENT_SIZE           = 99l;

  /** Test file size. */
  public static final long          FILE_SIZE                 = 98l;

  /** The Public WBB configuration file. */
  public static final String        CONFIG_FILE               = "./testdata/publicwbb.conf";

  /** The test Public WBB peer identifier. */
  public static final String        PEER                      = "PublicWBB1";

  /** Test sending peer. */
  public static final String        FROM_PEER                 = "Peer1";

  /** Test certificate alias. */
  public static final String        ALIAS                     = FROM_PEER + "_SigningSK1";

  /** The client key store. */
  public static final String        CLIENT_KEY_STORE          = "testdata/" + FROM_PEER + "/keys/" + FROM_PEER + "_private.bks";

  /** The client key store password. */
  public static final String        CLIENT_KEY_STORE_PASSWORD = "";

  /** SSL cipher suite. */
  public static final String        CIPHERS                   = "SSL_RSA_WITH_NULL_SHA";

  /** The listener address. */
  public static final String        LISTENER_ADDRESS          = "localhost";

  /** The external listening port. */
  public static final int           PORT                      = 8899;

  /** Test principal. */
  public static final X500Principal PRINCIPAL                 = new X500Principal("CN=" + FROM_PEER
                                                                  + ", OU=Dev, O=SurreyPAV, L=Guildford, S=Surrey, C=GB");

  /** Output certificate/key folder. */
  public static final String        CSR_FOLDER                = "new_certs";

  /** Output key store. */
  public static final String        OUTPUT_KEYSTORE           = CSR_FOLDER + "/" + "keystore.jks";

  /** Test upper buffer bound. */
  public static final int           BUFFER_BOUND              = 10240;

  /** Dummy exception message used for testing. */
  public static final String        EXCEPTION_MESSAGE         = "Exception Message";

  /** Test string data. */
  public static final String        LINE                      = "123456789\n";

  /** Test UUID/session ID. */
  public static final UUID          SESSION_ID                = UUID.randomUUID();

  /** Test device. */
  public static final String        DEVICE                    = "TestDeviceOne";

  /** Test serial number. */
  public static final String        SERIAL_NO                 = DEVICE + ":1";

  /** Test certificate. */
  public static final String        SENDER_CERTIFICATE        = "Peer1_SigningSK1";

  /** Test message as string. */
  public static final String        MESSAGE                   = "{\""
                                                                  + MessageFields.JSONWBBMessage.ID
                                                                  + "\":\""
                                                                  + SERIAL_NO
                                                                  + "\",\""
                                                                  + MessageFields.UUID
                                                                  + "\":\""
                                                                  + SESSION_ID
                                                                  + "\",\""
                                                                  + MessageFields.VoteMessage.RACES
                                                                  + "\":[{\"id\":\"LA\",\"preferences\":[\"1\",\" \",\"2\"]},{\"id\":\"LC_ATL\",\"preferences\":[\" \",\" \",\" \"]},{\"id\":\"LC_BTL\",\"preferences\":[\"1\",\" \",\" \"]}]}";

  /** Sleep interval for waiting. */
  private static final long         SLEEP_INTERVAL            = 1000;

  /** The database name. */
  private static final String       TEST_DATABASE             = "PublicWBB";

  /** Default buffer size to use when copying files. */
  private static final int          BUFFER_SIZE               = 8192;

  /** The singleton instance of these parameters. */
  private static TestParameters     instance                  = null;

  /** The dummy Public WBB peer instance. */
  private DummyPublicWBB            publicWBB                 = null;

  /** The test MongoDB. */
  private MongoClient               mongoClient               = null;

  /** The test MongoDB database. */
  private DB                        db                        = null;

  /** The SSL socket factory to make client connections. */
  private SSLSocketFactory          clientFactory             = null;

  /**
   * Private default constructor to prevent instantiation.
   */
  private TestParameters() {
    super();
  }

  /**
   * Closes the MongoDB.
   */
  public void closeDB() {
    if (this.mongoClient != null) {
      this.mongoClient.close();
      this.mongoClient = null;
      this.db = null;
    }
  }

  /**
   * Initialises and returns the SSL socket factory needed to create client SSL connections.
   * 
   * @return The SSL socket factory.
   * @throws PeerSSLInitException
   * @throws KeyManagementException
   * @throws NoSuchAlgorithmException
   */
  public SSLSocketFactory getClientSSLSocketFactory() throws PeerSSLInitException, KeyManagementException, NoSuchAlgorithmException {
    // Lazy creation.
    if (this.clientFactory == null) {
      String pathToKS = TestParameters.CLIENT_KEY_STORE;
      char[] pwd = TestParameters.CLIENT_KEY_STORE_PASSWORD.toCharArray();

      // Initialise the trust store for the client key.
      FileInputStream keyStoreStream = null;
      TrustManagerFactory tmf = null;
      KeyManagerFactory kmf = null;

      try {
        TVSKeyStore keyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
        keyStore.load(pathToKS, pwd);
        

        tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(keyStore.getKeyStore());

        kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        kmf.init(keyStore.getKeyStore(), pwd);
      }
      catch (Exception e) {
        throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores", e);
      }
      finally {
        if (keyStoreStream != null) {
          try {
            keyStoreStream.close();
          }
          catch (IOException ie) {
            throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores (Closing File)", ie);
          }
        }
      }

      // Create the client socket factory using the trust store.
      SSLContext context = SSLContext.getInstance("TLS");
      context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
      this.clientFactory = context.getSocketFactory();
    }

    return this.clientFactory;
  }

  /**
   * @return An encoded hash on the specified data.
   * @throws NoSuchAlgorithmException
   */
  public String getHash(String data) throws NoSuchAlgorithmException {
    MessageDigest digest = MessageDigest.getInstance("SHA1");

    digest.update(data.getBytes());

    return IOUtils.encodeData(EncodingType.BASE64, digest.digest());
  }

  /**
   * @return The test Public WBB peer.
   * @throws ServerInitException
   * @throws JSONIOException
   * @throws IOException 
   */
  public DummyPublicWBB getPublicWbbPeer() throws JSONIOException, ServerInitException, IOException {
    // Lazy creation.
    if (this.publicWBB == null) {
      this.publicWBB = new DummyPublicWBB(CONFIG_FILE);
    }

    return this.publicWBB;
  }

  /**
   * @return The test MongoDB database.
   * @throws UnknownHostException
   */
  public DB openDB() throws UnknownHostException {
    // Lazy creation.
    if (this.db == null) {
      this.mongoClient = new MongoClient();
      this.db = this.mongoClient.getDB(TEST_DATABASE);
    }

    return this.db;
  }

  /**
   * Copies the source file to the destination.
   * 
   * @param source
   *          The source file.
   * @param destination
   *          The destination file.
   * @throws IOException
   * @throws FileNotFoundException
   */
  public static void copyFile(File source, File destination) throws FileNotFoundException, IOException {
    copyStream(new FileInputStream(source), new FileOutputStream(destination));
  }

  /**
   * Copies an input stream to an output stream. Both streams are left at the end of their read/write.
   * 
   * @param in
   *          The destination stream.
   * @param out
   *          The source stream.
   * 
   * @throws IOException
   *           on failure to read or write.
   */
  public static void copyStream(InputStream in, OutputStream out) throws IOException {
    byte[] buffer = new byte[BUFFER_SIZE];

    int numBytes = in.read(buffer);

    while (numBytes != -1) {
      out.write(buffer, 0, numBytes);
      numBytes = in.read(buffer);
    }
  }

  /**
   * Creates and populates a test commit file and attachment.
   * 
   * @param commitFile
   *          The commit file.
   * @param attachmentFile
   *          The zipped attachment file. May be null if no attachment is required.
   * @return An encoded hash of the content.
   * @throws IOException
   * @throws CryptoIOException
   * @throws TVSSignatureException
   * @throws JSONException
   * @throws NoSuchAlgorithmException
   * @throws MessageJSONException
   * @throws KeyStoreException 
   */
  public static String createTestCommitFiles(String commitFile, String attachmentFile) throws IOException, TVSSignatureException,
      CryptoIOException, JSONException, NoSuchAlgorithmException, MessageJSONException, KeyStoreException {
    MessageDigest digest = MessageDigest.getInstance("SHA1");

    // Create a valid FileMessage.
    String messageDigest = TestParameters.signData(TestParameters.SERIAL_NO, null, FROM_PEER);
    String data = TestParameters.SERIAL_NO + messageDigest;

    JSONObject object = new JSONObject(TestParameters.MESSAGE);
    object.put(MessageFields.FileMessage.ID, TestParameters.SERIAL_NO);
    object.put(MessageFields.TYPE, "file");
    object.put(MessageFields.FileMessage.FILESIZE, 99);
    object.put(MessageFields.FileMessage.INTERNAL_FILENAME, commitFile);
    object.put(MessageFields.FileMessage.INTERNAL_DIGEST, messageDigest);
    object.put(MessageFields.FileMessage.DIGEST, messageDigest);
    object.put(MessageFields.JSONWBBMessage.COMMIT_TIME, TestParameters.COMMIT_TIME);
    object.put(MessageFields.JSONWBBMessage.SENDER_ID, TestParameters.SENDER_CERTIFICATE);
    object.put(MessageFields.JSONWBBMessage.SENDER_SIG, TestParameters.signData(data, null, FROM_PEER));

    FileMessage fileMessage = new FileMessage(object);

    digest.update(fileMessage.getInternalSignableContent().getBytes());

    // Save the message to file.
    IOUtils.writeStringToFile(fileMessage.getMsg().toString() + "\n", new File(commitFile).getAbsolutePath());

    // If an attachment is required, create as a zip file of the file that has just been created.
    if (attachmentFile != null) {
      ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(attachmentFile));
      ZipUtil.addFileToZip(new File(commitFile), digest, zos);
      zos.close();
    }

    return IOUtils.encodeData(EncodingType.BASE64, digest.digest());
  }

  /**
   * Recursive method to delete a folder and its content. If something goes wrong, this method remains silent.
   * 
   * @param file
   *          The file/folder to delete.
   */
  public static void deleteRecursive(File file) {
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        deleteRecursive(child);
      }
    }

    file.delete();
  }

  /**
   * Singleton access method.
   * 
   * @return The test parameters singleton instance.
   */
  public static TestParameters getInstance() {
    // Lazy creation.
    if (instance == null) {
      instance = new TestParameters();
    }

    return instance;
  }

  /**
   * Signs the specified content using the specified peer's SK2 key.
   * 
   * @param data1
   *          String data to sign.
   * @param data2
   *          Byte data to sign.
   * @param peer
   *          The peer name.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   * @throws KeyStoreException 
   */
  public static String signData(String data1, byte[] data2, String peer) throws TVSSignatureException, CryptoIOException, KeyStoreException {
    TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
    tvsKeyStore.load("./testdata/" + peer + "/keys/" + peer + "_private.bks", "".toCharArray());
    
    BLSPrivateKey key = tvsKeyStore.getBLSPrivateKey(peer + "_SigningSK2");
    TVSSignature signature = new TVSSignature(SignatureType.BLS, key);

    signature.update(data1);

    if (data2 != null) {
      signature.update(data2);
    }

    return signature.signAndEncode(EncodingType.BASE64);
  }

  /**
   * Deletes the test MongoDB database.
   * 
   * @throws UnknownHostException
   */
  public static void tidyDatabase() throws UnknownHostException {
    MongoClient mongoClient = new MongoClient();
    mongoClient.dropDatabase(TEST_DATABASE);
    mongoClient.close();
  }

  /**
   * Deletes all of the test output files that are generated.
   */
  public static void tidyFiles() {
    deleteRecursive(new File(CSR_FOLDER));
    deleteRecursive(new File(UPLOAD_FOLDER));
    deleteRecursive(new File(JSON_PATH));
    deleteRecursive(new File(ATTACHMENT_PATH));
  }

  /**
   * Closes all Public WBB socket created during testing.
   * 
   * @throws ServerInitException
   * @throws JSONIOException
   * @throws IOException
   */
  public static void tidySockets() throws IOException, JSONIOException, ServerInitException {
    // Shutdown the socket used by the peer so we can re-create them in the next test.
    if (getInstance().publicWBB != null) {
      getInstance().getPublicWbbPeer().getExternalServerSocket().close();
    }
  }

  /**
   * Wait for the specified number of loops.
   * 
   * @param count
   *          The number of loops.
   * @return
   */
  public static void wait(int count) {
    for (int i = 0; i < count; i++) {
      try {
        // Sleep for the interval.
        Thread.sleep(TestParameters.SLEEP_INTERVAL);
      }
      catch (InterruptedException e) {
        // Do nothing - we will run again.
      }
    }
  }
}
