/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.publicwbb.exceptions.AlreadyReceivedMessageException;

/**
 * Dummy test harness for the DB wrapper.
 */
public class DummyDBWrapper extends DBWrapper {

  /** Map containing values which say whether a method has been called. */
  private Map<String, Boolean> methodCalled  = new HashMap<String, Boolean>();

  /** Map containing parameter values passed into methods. */
  private Map<String, Object>  parameters    = new HashMap<String, Object>();

  /** Map of methods to exceptions that should be raised. */
  private Map<String, Integer> exceptionsMap = new HashMap<String, Integer>();

  /** Map containing return values for methods. */
  private Map<String, Object>  returns       = new HashMap<String, Object>();

  /**
   * Constructs a new DBWrapper to access the underlying data
   */
  public DummyDBWrapper() {
    super();
  }

  /**
   * Adds commit data to the set of data received for a particular commit time. Dummy test stub.
   * 
   * @param commitTime
   *          String commit time the data is in relation to
   * @param hash
   *          String hash of data
   * @param filePath
   *          String file path of the actual data file
   * @throws AlreadyReceivedMessageException
   * 
   * @see uk.ac.surrey.cs.tvs.publicwbb.DBWrapper#addCommitData(java.lang.String, java.lang.String, java.lang.String,
   *      java.lang.String)
   */
  @Override
  public void addCommitData(String commitTime, String hash, String filePathJSON, String filePathAttachments)
      throws AlreadyReceivedMessageException {
    this.methodCalled.put("addCommitData", true);
    this.parameters.put("commitTime", commitTime);
    this.parameters.put("hash", hash);
    this.parameters.put("filePathJSON", filePathJSON);
    this.parameters.put("filePathAttachments", filePathAttachments);

    Integer exception = this.exceptionsMap.get("addCommitData");
    if (exception != null) {
      switch (exception) {
        case 0:
          throw new AlreadyReceivedMessageException("test exception");
      }
    }
  }

  /**
   * Adds an exception to be raised for a method.
   * 
   * @param method
   *          The method to raise the exception.
   * @param exception
   *          The exception index to raise.
   */
  public void addException(String method, int exception) {
    this.exceptionsMap.put(method, exception);
  }

  /**
   * Adds a return value for a method.
   * 
   * @param method
   *          The method for the return value.
   * @param value
   *          The return value.
   */
  public void addReturn(String method, Object value) {
    this.returns.put(method, value);
  }

  /**
   * Adds a valid commit message to the database. Dummy test stub.
   * 
   * @param commit
   *          CommitMessage to add to the database
   * @throws JSONException
   * @throws AlreadyReceivedMessageException
   * 
   * @see uk.ac.surrey.cs.tvs.publicwbb.DBWrapper#addValidCommit(uk.ac.surrey.cs.tvs.publicwbb.CommitMessage)
   */
  @Override
  public void addValidCommit(CommitMessage commit) throws JSONException, AlreadyReceivedMessageException {
    this.methodCalled.put("addValidCommit", true);
    this.parameters.put("commit", commit);

    Integer exception = this.exceptionsMap.get("addValidCommit");
    if (exception != null) {
      switch (exception) {
        case 0:
          throw new JSONException("test exception");
        case 1:
          throw new AlreadyReceivedMessageException("test exception");
      }
    }
  }

  /**
   * @param parameter
   *          The parameter to check.
   * @return The last value of the specified parameter received, or null if not received.
   */
  public Object getParameter(String parameter) {
    return this.parameters.get(parameter);
  }

  /**
   * Gets the messages that have been stored. Dummy test stub.
   * 
   * @param commitTime
   *          String commit time to get messages for
   * @return List<JSONObject> of individually valid messages received
   * @throws JSONException
   * 
   * @see uk.ac.surrey.cs.tvs.publicwbb.DBWrapper#getValidReceivedMessages(java.lang.String)
   */
  @SuppressWarnings("unchecked")
  @Override
  public List<JSONObject> getValidReceivedMessages(String commitTime) throws JSONException {
    this.methodCalled.put("getValidReceivedMessages", true);
    this.parameters.put("commitTime", commitTime);

    Integer exception = this.exceptionsMap.get("getValidReceivedMessages");
    if (exception != null) {
      switch (exception) {
        case 0:
          throw new JSONException("test exception");
      }
    }

    return (List<JSONObject>) this.returns.get("getValidReceivedMessages");
  }

  /**
   * Checks whether we have already stored commit data for this commitTime and hash. Dummy test stub.
   * 
   * @param commitTime
   *          String commit time to check
   * @param hash
   *          String hash of data
   * @return boolean if we already have a record of that data, false if we do not
   * 
   * @see uk.ac.surrey.cs.tvs.publicwbb.DBWrapper#haveCommitData(java.lang.String, java.lang.String)
   */
  @Override
  public boolean haveCommitData(String commitTime, String hash) {
    this.methodCalled.put("haveCommitData", true);
    this.parameters.put("commitTime", commitTime);
    this.parameters.put("hash", hash);

    return (boolean) this.returns.get("haveCommitData");
  }

  /**
   * @param method
   *          The method to check.
   * @return Whether the method has been called.
   */
  public boolean isMethodCalled(String method) {
    boolean result = false;
    Boolean value = this.methodCalled.get(method);

    if (value != null) {
      result = value;
    }

    return result;
  }
}
