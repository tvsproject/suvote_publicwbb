/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.publicwbb.exceptions.AlreadyReceivedMessageException;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

/**
 * The class <code>DBWrapperTest</code> contains tests for the class <code>{@link DBWrapper}</code>.
 */
public class DBWrapperTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();

    // Tidy up the database.
    TestParameters.tidyDatabase();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the database.
    TestParameters.tidyDatabase();

    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void addCommitData(String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddCommitData_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    DBWrapper fixture = new DBWrapper();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    // Test no commit.
    fixture.addCommitData(TestParameters.COMMIT_TIME, hash, TestParameters.JSON_PATH, TestParameters.ATTACHMENT_PATH);

    DBCollection collection = db.getCollection(TestParameters.COMMITS);
    BasicDBObject query = new BasicDBObject(DBFields.ID, TestParameters.COMMIT_TIME);
    DBObject result = collection.findOne(query);

    BasicDBList list = (BasicDBList) result.get(DBFields.COMMIT_DATA_FILES);

    Iterator<Object> itr = list.iterator();

    while (itr.hasNext()) {
      BasicDBObject entry = (BasicDBObject) itr.next();

      assertTrue(entry.containsField(DBFields.DATA_HASH));
      assertTrue(entry.containsField(DBFields.FILE_PATH_JSON));
      assertTrue(entry.containsField(DBFields.FILE_PATH_ATTACHMENTS));

      assertEquals(hash, entry.get(DBFields.DATA_HASH));
      assertEquals(TestParameters.JSON_PATH, entry.get(DBFields.FILE_PATH_JSON));
      assertEquals(TestParameters.ATTACHMENT_PATH, entry.get(DBFields.FILE_PATH_ATTACHMENTS));
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void addCommitData(String,String,String,String) method test.
   * 
   * @throws Exception
   */
  @Test(expected = AlreadyReceivedMessageException.class)
  public void testAddCommitData_2() throws Exception {
    DBWrapper fixture = new DBWrapper();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    // Test existing commit.
    fixture.addCommitData(TestParameters.COMMIT_TIME, hash, TestParameters.JSON_PATH, TestParameters.ATTACHMENT_PATH);
    fixture.addCommitData(TestParameters.COMMIT_TIME, hash, TestParameters.JSON_PATH, TestParameters.ATTACHMENT_PATH);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void addValidCommit(CommitMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddValidCommit_1() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    DBWrapper fixture = new DBWrapper();

    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    CommitMessage commit = new CommitMessage(message.toString());

    // Test no commit.
    fixture.addValidCommit(commit);

    DBCollection collection = db.getCollection(TestParameters.COMMITS);
    BasicDBObject query = new BasicDBObject(DBFields.ID, TestParameters.COMMIT_TIME);
    DBObject result = collection.findOne(query);

    BasicDBList list = (BasicDBList) result.get(DBFields.VALID_MSGS_RECEIVED);

    Iterator<Object> itr = list.iterator();

    while (itr.hasNext()) {
      BasicDBObject entry = (BasicDBObject) itr.next();

      assertTrue(entry.containsField(DBFields.FROM_PEER));
      assertTrue(entry.containsField(DBFields.MSG));

      assertEquals(TestParameters.FROM_PEER, entry.get(DBFields.FROM_PEER));
      assertEquals(commit.getMsgString(), entry.get(DBFields.MSG));
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void addValidCommit(CommitMessage) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddValidCommit_2() throws Exception {
    DB db = TestParameters.getInstance().openDB();
    DBWrapper fixture = new DBWrapper();

    // Test multiple peers.
    String[] peers = new String[] { TestParameters.FROM_PEER, TestParameters.PEER };

    for (int i = 0; i < peers.length; i++) {
      JSONObject message = new JSONObject();

      String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

      String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
      byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
      String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

      message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
      message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
      message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
      message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
      message.put(MessageFields.FROM_PEER, peers[i]);
      message.put(MessageFields.CommitSK2.PEER_ID, peers[i]);
      message.put(MessageFields.CommitSK2.HASH, hash);
      message.put(MessageFields.CommitSK2.PEER_SIG, signature);

      CommitMessage commit = new CommitMessage(message.toString());

      // Test no commit.
      fixture.addValidCommit(commit);

      DBCollection collection = db.getCollection(TestParameters.COMMITS);
      BasicDBObject query = new BasicDBObject(DBFields.ID, TestParameters.COMMIT_TIME);
      DBObject result = collection.findOne(query);

      BasicDBList list = (BasicDBList) result.get(DBFields.VALID_MSGS_RECEIVED);

      Iterator<Object> itr = list.iterator();
      int count = 0;

      while (itr.hasNext()) {
        BasicDBObject entry = (BasicDBObject) itr.next();

        assertTrue(entry.containsField(DBFields.FROM_PEER));
        assertTrue(entry.containsField(DBFields.MSG));

        assertEquals(peers[count], entry.get(DBFields.FROM_PEER));
        if (count == i) {
          assertEquals(commit.getMsgString(), entry.get(DBFields.MSG));
        }

        count++;
      }
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the void addValidCommit(CommitMessage) method test.
   * 
   * @throws Exception
   */
  @Test(expected = AlreadyReceivedMessageException.class)
  public void testAddValidCommit_3() throws Exception {
    DBWrapper fixture = new DBWrapper();

    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    CommitMessage commit = new CommitMessage(message.toString());

    // Test existing commit.
    fixture.addValidCommit(commit);
    fixture.addValidCommit(commit);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the DBWrapper() constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testDBWrapper_1() throws Exception {
    DBWrapper result = new DBWrapper();
    assertNotNull(result);

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the List<JSONObject> getValidReceivedMessages(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetValidReceivedMessages_1() throws Exception {
    DBWrapper fixture = new DBWrapper();

    // Test no commit.
    List<JSONObject> result = fixture.getValidReceivedMessages(TestParameters.COMMIT_TIME);
    assertNotNull(result);

    assertEquals(0, result.size());

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the List<JSONObject> getValidReceivedMessages(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetValidReceivedMessages_2() throws Exception {
    DBWrapper fixture = new DBWrapper();

    String[] peers = new String[] { TestParameters.FROM_PEER, TestParameters.PEER };

    for (int i = 0; i < peers.length; i++) {
      JSONObject message = new JSONObject();

      String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

      String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
      byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
      String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

      message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
      message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
      message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
      message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
      message.put(MessageFields.FROM_PEER, peers[i]);
      message.put(MessageFields.CommitSK2.PEER_ID, peers[i]);
      message.put(MessageFields.CommitSK2.HASH, hash);
      message.put(MessageFields.CommitSK2.PEER_SIG, signature);

      CommitMessage commit = new CommitMessage(message.toString());

      fixture.addValidCommit(commit);

      // Test commit messages.
      List<JSONObject> result = fixture.getValidReceivedMessages(TestParameters.COMMIT_TIME);
      assertNotNull(result);

      assertEquals(i + 1, result.size());

      int count = 0;

      for (JSONObject response : result) {
        if (count == i) {
          assertEquals(message.toString(), response.toString());
        }

        count++;
      }
    }

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean haveCommitData(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testHaveCommitData_1() throws Exception {
    DBWrapper fixture = new DBWrapper();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    // Test no commit.
    assertFalse(fixture.haveCommitData(TestParameters.COMMIT_TIME, hash));

    TestParameters.getInstance().closeDB();
  }

  /**
   * Run the boolean haveCommitData(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testHaveCommitData_2() throws Exception {
    DBWrapper fixture = new DBWrapper();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    // Test existing commit.
    fixture.addCommitData(TestParameters.COMMIT_TIME, hash, TestParameters.JSON_PATH, TestParameters.ATTACHMENT_PATH);

    assertTrue(fixture.haveCommitData(TestParameters.COMMIT_TIME, hash));

    TestParameters.getInstance().closeDB();
  }
}