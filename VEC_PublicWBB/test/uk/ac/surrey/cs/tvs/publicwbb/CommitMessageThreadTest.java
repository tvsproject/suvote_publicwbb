/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.security.Principal;
import java.security.cert.Certificate;
import java.util.ArrayList;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.SSLSocket;
import javax.security.cert.X509Certificate;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>CommitMessageThreadTest</code> contains tests for the class <code>{@link CommitMessageThread}</code>.
 */
public class CommitMessageThreadTest {

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends SSLSocket {

    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

    private ByteArrayInputStream  inputStream  = null;

    private boolean               closed       = false;

    public DummySocket(String input) {
      super();

      if (input == null) {
        this.inputStream = new ByteArrayInputStream(new byte[0]);
      }
      else {
        this.inputStream = new ByteArrayInputStream(input.getBytes());
      }
    }

    public DummySocket(String input, File[] files) throws IOException {
      super();

      int length = input.getBytes().length;

      for (File file : files) {
        length += file.length();
      }

      byte[] data = new byte[length];
      byte[] inputBytes = input.getBytes();

      for (int i = 0; i < inputBytes.length; i++) {
        data[i] = inputBytes[i];
      }

      int offset = inputBytes.length;

      for (int i = 0; i < files.length; i++) {
        FileInputStream in = new FileInputStream(files[i]);
        in.read(data, offset, (int) files[i].length());
        in.close();
        offset += (int) files[i].length();
      }

      this.inputStream = new ByteArrayInputStream(data);
    }

    @Override
    public void addHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public String[] getEnabledCipherSuites() {
      return null;
    }

    @Override
    public String[] getEnabledProtocols() {
      return null;
    }

    @Override
    public boolean getEnableSessionCreation() {
      return false;
    }

    @Override
    public InputStream getInputStream() throws IOException {
      return this.inputStream;
    }

    @Override
    public boolean getNeedClientAuth() {
      return false;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      return this.outputStream;
    }

    public String getOutputStreamData() {
      return this.outputStream.toString();
    }

    @Override
    public SocketAddress getRemoteSocketAddress() {
      return new InetSocketAddress(TestParameters.LISTENER_ADDRESS, TestParameters.PORT);
    }

    @Override
    public SSLSession getSession() {
      return new SSLSession() {

        @Override
        public int getApplicationBufferSize() {
          return 0;
        }

        @Override
        public String getCipherSuite() {
          return null;
        }

        @Override
        public long getCreationTime() {
          return 0;
        }

        @Override
        public byte[] getId() {
          return null;
        }

        @Override
        public long getLastAccessedTime() {
          return 0;
        }

        @Override
        public Certificate[] getLocalCertificates() {
          return null;
        }

        @Override
        public Principal getLocalPrincipal() {
          return null;
        }

        @Override
        public int getPacketBufferSize() {
          return 0;
        }

        @Override
        public X509Certificate[] getPeerCertificateChain() throws SSLPeerUnverifiedException {
          return null;
        }

        @Override
        public Certificate[] getPeerCertificates() throws SSLPeerUnverifiedException {
          return null;
        }

        @Override
        public String getPeerHost() {
          return null;
        }

        @Override
        public int getPeerPort() {
          return 0;
        }

        @Override
        public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
          return TestParameters.PRINCIPAL;
        }

        @Override
        public String getProtocol() {
          return null;
        }

        @Override
        public SSLSessionContext getSessionContext() {
          return null;
        }

        @Override
        public Object getValue(String name) {
          return null;
        }

        @Override
        public String[] getValueNames() {
          return null;
        }

        @Override
        public void invalidate() {
        }

        @Override
        public boolean isValid() {
          return false;
        }

        @Override
        public void putValue(String name, Object value) {
        }

        @Override
        public void removeValue(String name) {
        }
      };
    }

    @Override
    public String[] getSupportedCipherSuites() {
      return null;
    }

    @Override
    public String[] getSupportedProtocols() {
      return null;
    }

    @Override
    public boolean getUseClientMode() {
      return false;
    }

    @Override
    public boolean getWantClientAuth() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }

    @Override
    public void removeHandshakeCompletedListener(HandshakeCompletedListener listener) {
    }

    @Override
    public void setEnabledCipherSuites(String[] suites) {
    }

    @Override
    public void setEnabledProtocols(String[] protocols) {
    }

    @Override
    public void setEnableSessionCreation(boolean flag) {
    }

    @Override
    public void setNeedClientAuth(boolean need) {
    }

    @Override
    public void setUseClientMode(boolean mode) {
    }

    @Override
    public void setWantClientAuth(boolean want) {
    }

    @Override
    public void startHandshake() throws IOException {
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();

    // Close the opened sockets.
    TestParameters.tidySockets();
  }

  /**
   * Run the CommitMessageThread(PublicWBB,SSLSocket) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitMessageThread_1() throws Exception {
    SSLSocket socket = new DummySocket(TestParameters.LINE);

    CommitMessageThread result = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);
    assertNotNull(result);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    SSLSocket socket = new DummySocket(null);

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test an empty message.
    fixture.run();

    assertTrue(socket.isClosed());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_10() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.createTestCommitFiles(TestParameters.JSON_PATH, TestParameters.ATTACHMENT_PATH);

    File commitFile = new File(TestParameters.JSON_PATH);
    File attachementFile = new File(TestParameters.ATTACHMENT_PATH);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, attachementFile.length());
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, commitFile.length());
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    DummySocket socket = new DummySocket(message.toString() + "\n", new File[] { commitFile, attachementFile });

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test a valid message, with files and valid hash but already received.
    DummyDBWrapper db = new DummyDBWrapper();
    TestParameters.getInstance().getPublicWbbPeer().setDB(db);

    db.addReturn("haveCommitData", false);
    db.addException("addCommitData", 0);
    //CJC Added the following
    ArrayList<JSONObject> validReturn = new ArrayList<JSONObject>();
    validReturn.add(message);
    db.addReturn("getValidReceivedMessages", validReturn);
    //CJC End
    fixture.run();

    assertTrue(db.isMethodCalled("addValidCommit"));
    assertEquals(new CommitMessage(message.toString()).getMsgString(), ((CommitMessage) db.getParameter("commit")).getMsgString());

    assertTrue(db.isMethodCalled("haveCommitData"));
    assertEquals(TestParameters.COMMIT_TIME, db.getParameter("commitTime"));
    assertEquals(hash, db.getParameter("hash"));

    assertTrue(db.isMethodCalled("addCommitData"));
    assertEquals(TestParameters.COMMIT_TIME, db.getParameter("commitTime"));
    assertEquals(hash, db.getParameter("hash"));
    assertNotNull(db.getParameter("filePathJSON"));
    assertNotNull(db.getParameter("filePathAttachments"));

    String response = socket.getOutputStreamData();
    assertNotNull(response);

    JSONObject result = new JSONObject(response);
    assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ACCEPTED_NO_DATA, result.getString(PublicWBBConstants.Messages.MSG_TYPE));

    assertTrue(socket.isClosed());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    SSLSocket socket = new DummySocket("\n");

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test an empty message.
    fixture.run();

    assertTrue(socket.isClosed());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_3() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME + "rubbish";
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    DummySocket socket = new DummySocket(message.toString() + "\n");

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test an invalid message.
    fixture.run();

    String response = socket.getOutputStreamData();
    assertNotNull(response);

    JSONObject result = new JSONObject(response);
    assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ERROR, result.getString(PublicWBBConstants.Messages.MSG_TYPE));

    assertTrue(socket.isClosed());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_4() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    DummySocket socket = new DummySocket(message.toString() + "\n");

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test a valid message but already received.
    DummyDBWrapper db = new DummyDBWrapper();
    TestParameters.getInstance().getPublicWbbPeer().setDB(db);

    db.addReturn("haveCommitData", true);
  //CJC Added the following
    ArrayList<JSONObject> validReturn = new ArrayList<JSONObject>();
    validReturn.add(message);
    db.addReturn("getValidReceivedMessages", validReturn);
    //CJC End
    fixture.run();

    assertTrue(db.isMethodCalled("addValidCommit"));
    assertEquals(new CommitMessage(message.toString()).getMsgString(), ((CommitMessage) db.getParameter("commit")).getMsgString());

    assertTrue(db.isMethodCalled("haveCommitData"));
    assertEquals(TestParameters.COMMIT_TIME, db.getParameter("commitTime"));
    assertEquals(hash, db.getParameter("hash"));

    String response = socket.getOutputStreamData();
    assertNotNull(response);

    JSONObject result = new JSONObject(response);
    assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ACCEPTED_NO_DATA, result.getString(PublicWBBConstants.Messages.MSG_TYPE));

    assertTrue(socket.isClosed());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_5() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    DummySocket socket = new DummySocket(message.toString() + "\n");

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test a valid message but already received.
    DummyDBWrapper db = new DummyDBWrapper();
    TestParameters.getInstance().getPublicWbbPeer().setDB(db);

    db.addException("addValidCommit", 1);
    //CJC: Added this because otherwise it returns null
    db.addReturn("haveCommitData", true);
    fixture.run();

    assertTrue(db.isMethodCalled("addValidCommit"));
    assertEquals(new CommitMessage(message.toString()).getMsgString(), ((CommitMessage) db.getParameter("commit")).getMsgString());

    String response = socket.getOutputStreamData();
    assertNotNull(response);

    JSONObject result = new JSONObject(response);
    assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ERROR, result.getString(PublicWBBConstants.Messages.MSG_TYPE));

    assertTrue(socket.isClosed());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_6() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    DummySocket socket = new DummySocket(message.toString() + "\n");

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test an invalid message.
    DummyDBWrapper db = new DummyDBWrapper();
    TestParameters.getInstance().getPublicWbbPeer().setDB(db);

    db.addException("addValidCommit", 0);
    db.addReturn("haveCommitData", false);
    fixture.run();
    //CJC Removed this because we now don't call addValidCommit on a message unless it is valid
    assertFalse(db.isMethodCalled("addValidCommit"));
    //assertEquals(new CommitMessage(message.toString()).getMsgString(), ((CommitMessage) db.getParameter("commit")).getMsgString());

    String response = socket.getOutputStreamData();
    assertNotNull(response);

    JSONObject result = new JSONObject(response);
    assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ERROR, result.getString(PublicWBBConstants.Messages.MSG_TYPE));

    assertTrue(socket.isClosed());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_7() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    DummySocket socket = new DummySocket(message.toString() + "\n");

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test a valid message, not already received, missing files.
    DummyDBWrapper db = new DummyDBWrapper();
    TestParameters.getInstance().getPublicWbbPeer().setDB(db);

    db.addReturn("haveCommitData", false);

    fixture.run();

    assertFalse(db.isMethodCalled("addValidCommit"));
    //assertEquals(new CommitMessage(message.toString()).getMsgString(), ((CommitMessage) db.getParameter("commit")).getMsgString());

    assertTrue(db.isMethodCalled("haveCommitData"));
    assertEquals(TestParameters.COMMIT_TIME, db.getParameter("commitTime"));
    assertEquals(hash, db.getParameter("hash"));

    String response = socket.getOutputStreamData();
    assertNotNull(response);

    JSONObject result = new JSONObject(response);
    assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ERROR, result.getString(PublicWBBConstants.Messages.MSG_TYPE));

    assertTrue(socket.isClosed());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_8() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash("");

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    DummySocket socket = new DummySocket(message.toString() + "\n");

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test a valid message, not already received, missing files but valid hash.
    DummyDBWrapper db = new DummyDBWrapper();
    TestParameters.getInstance().getPublicWbbPeer().setDB(db);

    db.addReturn("haveCommitData", false);
    //CJC Added the following
    ArrayList<JSONObject> validReturn = new ArrayList<JSONObject>();
    validReturn.add(message);
    db.addReturn("getValidReceivedMessages", validReturn);
    //CJC End
    fixture.run();

    assertTrue(db.isMethodCalled("addValidCommit"));
    assertEquals(new CommitMessage(message.toString()).getMsgString(), ((CommitMessage) db.getParameter("commit")).getMsgString());

    assertTrue(db.isMethodCalled("haveCommitData"));
    assertEquals(TestParameters.COMMIT_TIME, db.getParameter("commitTime"));
    assertEquals(hash, db.getParameter("hash"));

    assertTrue(db.isMethodCalled("addCommitData"));
    assertEquals(TestParameters.COMMIT_TIME, db.getParameter("commitTime"));
    assertEquals(hash, db.getParameter("hash"));
    assertNotNull(db.getParameter("filePathJSON"));
    assertNotNull(db.getParameter("filePathAttachments"));

    String response = socket.getOutputStreamData();
    assertNotNull(response);

    JSONObject result = new JSONObject(response);
    assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ACCEPTED, result.getString(PublicWBBConstants.Messages.MSG_TYPE));

    assertTrue(socket.isClosed());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_9() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.createTestCommitFiles(TestParameters.JSON_PATH, TestParameters.ATTACHMENT_PATH);

    File commitFile = new File(TestParameters.JSON_PATH);
    File attachementFile = new File(TestParameters.ATTACHMENT_PATH);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, attachementFile.length());
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, commitFile.length());
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    DummySocket socket = new DummySocket(message.toString() + "\n", new File[] { commitFile, attachementFile });

    CommitMessageThread fixture = new CommitMessageThread(TestParameters.getInstance().getPublicWbbPeer(), socket);

    // Test a valid message, not already received, with files and valid hash.
    DummyDBWrapper db = new DummyDBWrapper();
    TestParameters.getInstance().getPublicWbbPeer().setDB(db);

    db.addReturn("haveCommitData", false);
    //CJC Added the following
    ArrayList<JSONObject> validReturn = new ArrayList<JSONObject>();
    validReturn.add(message);
    db.addReturn("getValidReceivedMessages", validReturn);
    //CJC End
    fixture.run();

    assertTrue(db.isMethodCalled("addValidCommit"));
    assertEquals(new CommitMessage(message.toString()).getMsgString(), ((CommitMessage) db.getParameter("commit")).getMsgString());

    assertTrue(db.isMethodCalled("haveCommitData"));
    assertEquals(TestParameters.COMMIT_TIME, db.getParameter("commitTime"));
    assertEquals(hash, db.getParameter("hash"));

    assertTrue(db.isMethodCalled("addCommitData"));
    assertEquals(TestParameters.COMMIT_TIME, db.getParameter("commitTime"));
    assertEquals(hash, db.getParameter("hash"));
    assertNotNull(db.getParameter("filePathJSON"));
    assertNotNull(db.getParameter("filePathAttachments"));

    String response = socket.getOutputStreamData();
    assertNotNull(response);

    JSONObject result = new JSONObject(response);
    assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ACCEPTED, result.getString(PublicWBBConstants.Messages.MSG_TYPE));

    assertTrue(socket.isClosed());
  }
}