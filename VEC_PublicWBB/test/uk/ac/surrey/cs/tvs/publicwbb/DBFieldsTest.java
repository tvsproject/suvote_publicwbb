/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

/**
 * The class <code>DBFieldsTest</code> contains tests for the class <code>{@link DBFields}</code>.
 */
public class DBFieldsTest {

  /**
   * Run the DBFields() constructor test.
   */
  @Test
  public void testDBFields_1() throws Exception {
    DBFields result = new DBFields();
    assertNotNull(result);
  }
}