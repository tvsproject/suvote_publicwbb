/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import java.io.IOException;

import uk.ac.surrey.cs.tvs.publicwbb.exceptions.ServerInitException;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Dummy test harness for the Public WBB.
 */
public class DummyPublicWBB extends PublicWBB {

  /** Replacement DB. */
  private DBWrapper replacementDB = null;

  /**
   * Constructor requiring the configuration file.
   * 
   * @param configFile
   *          The peer configuration.
   * @throws JSONIOException
   * @throws ServerInitException
   * @throws IOException 
   */
  public DummyPublicWBB(String configFile) throws JSONIOException, ServerInitException, IOException {
    super(configFile);
  }

  /**
   * @return The concurrent database wrapper or the test stub, as required.
   * @see uk.ac.surrey.cs.tvs.publicwbb.PublicWBB#getDB()
   */
  @Override
  public DBWrapper getDB() {
    DBWrapper db = super.getDB();

    if (this.replacementDB != null) {
      db = this.replacementDB;
    }

    return db;
  }

  /**
   * @param db
   *          the replacement DB.
   */
  public void setDB(DBWrapper replacementDB) {
    this.replacementDB = replacementDB;
  }
}
