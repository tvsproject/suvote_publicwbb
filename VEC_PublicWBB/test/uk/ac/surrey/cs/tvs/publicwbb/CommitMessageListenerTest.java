/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;

import javax.net.ssl.SSLSocket;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The class <code>CommitMessageListenerTest</code> contains tests for the class <code>{@link CommitMessageListener}</code>.
 */
public class CommitMessageListenerTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();

    // Close the opened sockets.
    TestParameters.tidySockets();
  }

  /**
   * Run the CommitMessageListener(PublicWBB) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitMessageListener_1() throws Exception {
    CommitMessageListener result = new CommitMessageListener(TestParameters.getInstance().getPublicWbbPeer());
    assertNotNull(result);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    CommitMessageListener fixture = new CommitMessageListener(TestParameters.getInstance().getPublicWbbPeer());

    // Put the CommitMessageListener in a thread and start it.
    Thread thread = new Thread(fixture);
    thread.start();

    // Wait for the socket to start up.
    TestParameters.wait(1);

    // Connect to the socket and send some data.
    SSLSocket socket = (SSLSocket) TestParameters.getInstance().getClientSSLSocketFactory().createSocket();
    socket.setEnabledCipherSuites(new String[] { TestParameters.CIPHERS });
    socket.setUseClientMode(true);
    try{
    socket.connect(new InetSocketAddress(TestParameters.LISTENER_ADDRESS, TestParameters.PORT));
    }catch(IOException e){
      e.printStackTrace();
    }
    BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
    bos.write("{}\n".getBytes());
    bos.flush();

    // Wait for a response.
    BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
    byte[] buffer = new byte[TestParameters.BUFFER_BOUND];
    bis.read(buffer);

    String result = new String(buffer);

    bos.close();
    bis.close();

    socket.close();

    // We don't care what the response is, just that we got one. Responses will be tested elsewhere.
    JSONObject response = new JSONObject(result);
    assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ERROR, response.getString(PublicWBBConstants.Messages.MSG_TYPE));

    // Shutdown.
    fixture.shutdown();

    // Force the socket to close and unblock the accept.
    TestParameters.tidySockets();

    // Wait for the thread to finish.
    thread.join();
    assertFalse(thread.isAlive());
  }
}