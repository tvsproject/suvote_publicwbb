/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb.setup;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.publicwbb.TestParameters;

/**
 * The class <code>PublicWBBKeyGenerationTest</code> contains tests for the class <code>{@link PublicWBBKeyGeneration}</code>.
 */
public class PublicWBBKeyGenerationTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void generateKeyAndCSR(String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateKeyAndCSR_1() throws Exception {
    PublicWBBKeyGeneration fixture = new PublicWBBKeyGeneration(TestParameters.PEER);

    fixture.generateKeyAndCSR(TestParameters.CSR_FOLDER, TestParameters.OUTPUT_KEYSTORE);

    // Test certificate creation. We don't test the content.
    File csrFile = new File(TestParameters.CSR_FOLDER, TestParameters.PEER + ".csr");

    assertTrue(csrFile.exists());
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    String[] args = new String[] {};

    PublicWBBKeyGeneration.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    String[] args = new String[] { "rubbish" };

    PublicWBBKeyGeneration.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_3() throws Exception {
    String[] args = new String[] { "generate", TestParameters.PEER, TestParameters.CSR_FOLDER, TestParameters.OUTPUT_KEYSTORE };

    PublicWBBKeyGeneration.main(args);

    // Test certificate creation. We don't test the content.
    File csrFile = new File(TestParameters.CSR_FOLDER, TestParameters.PEER + ".csr");

    assertTrue(csrFile.exists());
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_4() throws Exception {
    String[] args = new String[] { "import", TestParameters.PEER, TestParameters.OUTPUT_KEYSTORE, TestParameters.CSR_FOLDER,
        "rubbish" };

    // Just test that something happens - the actual import is tested elsewhere.
    PublicWBBKeyGeneration.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_5() throws Exception {
    String[] args = new String[] { "generate", TestParameters.PEER, TestParameters.CSR_FOLDER, TestParameters.OUTPUT_KEYSTORE,
        "rubbish" };

    PublicWBBKeyGeneration.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_6() throws Exception {
    String[] args = new String[] { "import", TestParameters.PEER, TestParameters.OUTPUT_KEYSTORE, TestParameters.CSR_FOLDER,
        "rubbish", "rubbish" };

    // Just test that something happens - the actual import is tested elsewhere.
    PublicWBBKeyGeneration.main(args);
  }

  /**
   * Run the PublicWBBKeyGeneration(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPublicWBBKeyGeneration_1() throws Exception {
    PublicWBBKeyGeneration result = new PublicWBBKeyGeneration(TestParameters.PEER);
    assertNotNull(result);
  }
}