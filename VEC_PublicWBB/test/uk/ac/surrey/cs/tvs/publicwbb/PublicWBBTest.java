/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSCertificate;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>PublicWBBTest</code> contains tests for the class <code>{@link PublicWBB}</code>.
 */
public class PublicWBBTest {

  /**
   * Dummy socket for testing.
   */
  private class DummySocket extends Socket {

    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private boolean               closed       = false;

    @Override
    public synchronized void close() throws IOException {
      this.closed = true;
    }

    @Override
    public OutputStream getOutputStream() throws IOException {
      return this.outputStream;
    }

    public String getOutputStreamData() {
      return this.outputStream.toString();
    }

    @Override
    public boolean isClosed() {
      return this.closed;
    }
  }

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the old output files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the Certificate getCertificateFromCerts(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCertificateFromCerts_1() throws Exception {
    PublicWBB fixture = new PublicWBB(TestParameters.CONFIG_FILE);

    TVSCertificate result = fixture.getCertificateFromCerts(TestParameters.ALIAS);
    assertNotNull(result);

    fixture.getExternalServerSocket().close();
  }

  /**
   * Run the String getCertificateName(Principal) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetCertificateName_1() throws Exception {
    PublicWBB fixture = new PublicWBB(TestParameters.CONFIG_FILE);

    String result = fixture.getCertificateName(TestParameters.PRINCIPAL);
    assertNotNull(result);

    assertEquals(TestParameters.FROM_PEER, result);

    fixture.getExternalServerSocket().close();
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    // Copy the default configuration file so we can modify it and put it back.
    File config = new File("./testdata/publicwbb.conf");
    File save = new File("./testdata/save_publicwbb.conf");
    save.deleteOnExit();
    TestParameters.copyFile(config, save);
    try{
    // Modify the port to be something different to that used in all other tests. This is because the port isn't shut until the JVM
    // exits at the end of testing.
    JSONObject object = IOUtils.readJSONObjectFromFile(config.getAbsolutePath());
    int oldPort = object.getInt(PublicWBBConstants.Config.LISTEN_PORT);
    object.put(PublicWBBConstants.Config.LISTEN_PORT, oldPort + 1);
    IOUtils.writeJSONToFile(object, config.getAbsolutePath());

    String[] args = new String[] {"./testdata/publicwbb.conf"};

    PublicWBB.main(args);
    }finally{
    // Now put back the configuration.
    TestParameters.copyFile(save, config);
    }
  }

  /**
   * Run the PublicWBB(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testPublicWBB_1() throws Exception {
    PublicWBB result = new PublicWBB(TestParameters.CONFIG_FILE);
    assertNotNull(result);

    assertNotNull(result.getConfig());
    assertNotNull(result.getDB());
    assertNotNull(result.getUploadDir());
    assertNotNull(result.getExternalServerSocket());

    result.getExternalServerSocket().close();
  }

  /**
   * Run the void sendErrorMessage(PublicWBBCommitError,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendErrorMessage_1() throws Exception {
    PublicWBB fixture = new PublicWBB(TestParameters.CONFIG_FILE);

    for (PublicWBBCommitError error : PublicWBBCommitError.values()) {
      DummySocket socket = new DummySocket();

      fixture.sendErrorMessage(error, socket);

      String result = socket.getOutputStreamData();
      assertNotNull(result);

      JSONObject response = new JSONObject(result);

      assertTrue(response.has(PublicWBBConstants.Messages.MSG_TYPE));
      assertTrue(response.has(PublicWBBConstants.Messages.MSG));

      assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ERROR, response.getString(PublicWBBConstants.Messages.MSG_TYPE));
      assertNotNull(response.getString(PublicWBBConstants.Messages.MSG));

      assertTrue(socket.isClosed());
    }

    fixture.getExternalServerSocket().close();
  }

  /**
   * Run the void sendMessageAndClose(String,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendMessageAndClose_1() throws Exception {
    PublicWBB fixture = new PublicWBB(TestParameters.CONFIG_FILE);

    DummySocket socket = new DummySocket();

    fixture.sendMessageAndClose(TestParameters.LINE, socket);
    assertEquals(TestParameters.LINE + System.getProperty("line.separator"), socket.getOutputStreamData());
    assertTrue(socket.isClosed());

    fixture.getExternalServerSocket().close();
  }

  /**
   * Run the void sendResponseMessage(PublicWBBCommitResponse,Socket) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSendResponseMessage_1() throws Exception {
    PublicWBB fixture = new PublicWBB(TestParameters.CONFIG_FILE);

    for (PublicWBBCommitResponse responseType : PublicWBBCommitResponse.values()) {
      DummySocket socket = new DummySocket();

      fixture.sendResponseMessage(responseType, socket);

      String result = socket.getOutputStreamData();
      assertNotNull(result);

      JSONObject response = new JSONObject(result);

      assertTrue(response.has(PublicWBBConstants.Messages.MSG_TYPE));
      assertTrue(response.has(PublicWBBConstants.Messages.MSG));

      switch (responseType) {
        case ACCEPTED:
          assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ACCEPTED, response.getString(PublicWBBConstants.Messages.MSG_TYPE));
          break;
        case ACCEPTED_DATA_RECEIVED_ALREADY:
          assertEquals(PublicWBBConstants.Messages.MSG_TYPE_ACCEPTED_NO_DATA,
              response.getString(PublicWBBConstants.Messages.MSG_TYPE));
          break;
        default:
          break;
      }

      assertNotNull(response.getString(PublicWBBConstants.Messages.MSG));

      assertTrue(socket.isClosed());
    }

    fixture.getExternalServerSocket().close();
  }
}