/*******************************************************************************
 * Copyright (c) 2013 Coasca Limited.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.publicwbb;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.publicwbb.exceptions.MessageException;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>CommitMessageTest</code> contains tests for the class <code>{@link CommitMessage}</code>.
 */
public class CommitMessageTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Close the opened sockets.
    TestParameters.tidySockets();

    // Tidy up the old output files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Delete the output files.
    TestParameters.tidyFiles();

    // Close the opened sockets.
    TestParameters.tidySockets();
  }

  /**
   * Run the CommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageException.class)
  public void testCommitMessage_1() throws Exception {
    CommitMessage result = new CommitMessage("");
    assertNotNull(result);
  }

  /**
   * Run the CommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitMessage_2() throws Exception {
    CommitMessage result = new CommitMessage("{}");
    assertNotNull(result);
  }

  /**
   * Run the CommitMessage(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testCommitMessage_3() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);

    CommitMessage result = new CommitMessage(message.toString());
    assertNotNull(result);

    assertEquals(TestParameters.ATTACHMENT_SIZE, result.getAttachmentSize());
    assertEquals(TestParameters.COMMIT_TIME, result.getCommitID());
    assertEquals(TestParameters.FILE_SIZE, result.getFileSize());
    assertEquals(TestParameters.FROM_PEER, result.getFromPeer());
    assertEquals(hash, result.getHash());
    assertEquals(message.toString(), result.getMsgString());
  }

  /**
   * Run the long getAttachmentSize() method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONException.class)
  public void testGetAttachmentSize_1() throws Exception {
    CommitMessage fixture = new CommitMessage("{}");

    fixture.getAttachmentSize();
  }

  /**
   * Run the String getCommitTime() method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONException.class)
  public void testGetCommitTime_1() throws Exception {
    CommitMessage fixture = new CommitMessage("{}");

    fixture.getCommitID();
  }

  /**
   * Run the long getFileSize() method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONException.class)
  public void testGetFileSize_1() throws Exception {
    CommitMessage fixture = new CommitMessage("{}");

    fixture.getFileSize();
  }

  /**
   * Run the String getFromPeer() method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONException.class)
  public void testGetFromPeer_1() throws Exception {
    CommitMessage fixture = new CommitMessage("{}");

    fixture.getFromPeer();
  }

  /**
   * Run the String getHash() method test.
   * 
   * @throws Exception
   */
  @Test(expected = JSONException.class)
  public void testGetHash_1() throws Exception {
    CommitMessage fixture = new CommitMessage("{}");

    fixture.getHash();
  }

  /**
   * Run the boolean performValidation(PublicWBB) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_1() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME + "rubbish";
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    CommitMessage fixture = new CommitMessage(message.toString());

    assertFalse(fixture.performValidation(TestParameters.getInstance().getPublicWbbPeer()));
  }

  /**
   * Run the boolean performValidation(PublicWBB) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testPerformValidation_2() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.PEER_ID, TestParameters.FROM_PEER);
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    CommitMessage fixture = new CommitMessage(message.toString());

    assertTrue(fixture.performValidation(TestParameters.getInstance().getPublicWbbPeer()));
  }

  /**
   * Run the boolean performValidation(PublicWBB) method test.
   * 
   * @throws Exception
   */
  @Test(expected = MessageException.class)
  public void testPerformValidation_3() throws Exception {
    JSONObject message = new JSONObject();

    String hash = TestParameters.getInstance().getHash(TestParameters.LINE);

    String data1 = TestParameters.COMMIT_MESSAGE_TYPE + TestParameters.COMMIT_TIME;
    byte[] data2 = IOUtils.decodeData(EncodingType.BASE64, hash);
    String signature = TestParameters.signData(data1, data2, TestParameters.FROM_PEER);

    message.put(MessageFields.TYPE, TestParameters.COMMIT_MESSAGE_TYPE);
    message.put(MessageFields.CommitSK2.ATTACHMENT_SIZE, TestParameters.ATTACHMENT_SIZE);
    message.put(MessageFields.CommitSK2.COMMIT_TIME, TestParameters.COMMIT_TIME);
    message.put(MessageFields.CommitSK2.FILE_SIZE, TestParameters.FILE_SIZE);
    message.put(MessageFields.FROM_PEER, "rubbish");
    message.put(MessageFields.CommitSK2.PEER_ID, "rubbish");
    message.put(MessageFields.CommitSK2.HASH, hash);
    message.put(MessageFields.CommitSK2.PEER_SIG, signature);

    CommitMessage fixture = new CommitMessage(message.toString());

    assertTrue(fixture.performValidation(TestParameters.getInstance().getPublicWbbPeer()));
  }

  /**
   * Run the void setFromPeer(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetFromPeer_1() throws Exception {
    JSONObject message = new JSONObject();

    message.put(MessageFields.FROM_PEER, TestParameters.FROM_PEER);

    CommitMessage fixture = new CommitMessage(message.toString());
    assertEquals(TestParameters.FROM_PEER, fixture.getFromPeer());

    fixture.setFromPeer(TestParameters.PEER);
    assertEquals(TestParameters.PEER, fixture.getFromPeer());
  }
}